#include <iostream>
#include <cmath>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TCut.h>

#include "readCut.C"

using namespace std;

// TODO: integrate into Data Struct, like pedestal
// dy type cuts
float beamY(int roadset) {
    if (roadset >= 62) { // run 3
        return 1.6;
    }
    else if (roadset <= 59) { // run 2
        return 0.4;
    }
    else {
        return 0;
    }
    // run 4, 5, ...?
}
int beamPol(int roadset) {
    if (roadset <= 62) { // run 2 + rs62
        return -1;
    }
    else {
        return +1;
    }
}

int main(int argc, char* argv[])
{
    if (argc < 1 + 3) {
        printf("Usage: %s selection src_file out_file [roadset]\n", argv[0]);
        return EXIT_FAILURE;
    }
    TString selection = argv[1];
    const char * src_file = argv[2];
    const char * out_file = argv[3];
    const char * roadset_arg = argv[4];
    int roadset = -1;
    if (argc > 4) {
        roadset = atoi(roadset_arg);
    }
    // read cut from file if filename passed
    if (selection.EndsWith(".cut", TString::kIgnoreCase)
            || selection.EndsWith(".txt", TString::kIgnoreCase)
            || selection.EndsWith(".C")) {
        selection = readCut(selection);
    }

    // correct for beam offset
    if (roadset >= 0) {
        //float yoffs = beamY(roadset);
        //int pol = beamPol(roadset);
        //cout << Form("Adjusting for data set: beamY=%f / pol=%d", yoffs, pol) << endl;
        cout << "Adjusting for data set with builtin functions" << endl;
        //selection.ReplaceAll("$YOFFS", Form("(%E)", yoffs));
        //selection.ReplaceAll("$POL", Form("(%d)", pol));
        selection.ReplaceAll("$YOFFS", "BeamY()");
        selection.ReplaceAll("$POL", "polarity()");
    }

    // Get old file and old tree
    TFile *oldfile = new TFile(src_file, "read");
    TTree *oldtree = (TTree*)oldfile->Get("save");
    //Long64_t nentries = oldtree->GetEntries();

    // Create a new file and make cut of old tree in new file
    cout << "Applying cut: " << selection << endl;
    TFile *newfile = new TFile(out_file, "new");
    TTree *newtree = oldtree->CopyTree(selection);

    cout << "Writing to file: " << out_file << endl;
    newtree->AutoSave();
    delete oldfile;
    delete newfile;

    return EXIT_SUCCESS;
}
