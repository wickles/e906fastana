#include <iostream>
#include <cmath>
#include <stdlib.h>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TSQLServer.h>
#include <TSQLResult.h>
#include <TSQLRow.h>

#define GOBJECT Event
#include "sqlUtil.h"
#include "DataStruct.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 4) {
        // not enough arguments
        printf("Usage: %s server schema output_file\n", argv[0]);
        return -1;
    }
    // collect args
    TString server_name = argv[1];
    TString schema_main = argv[2];
    TString fout_dat = argv[3];

    // define the output file structure
    Event* p_event = new Event; Event& event = *p_event;
    g906 = p_event;

    TFile* saveFile;
    TTree* saveTree;

    //Connect to server
    TSQLServer* server = TSQLServer::Connect(Form("mysql://%s", server_name.Data()), seaquel_user, seaquel_passwd);
    server->Exec(Form("USE %s", schema_main.Data()));
    cout << "Reading schema " << schema_main << " and save to " << fout_dat << "_(runID).root" << endl;

    TString query = "SELECT runID,spillID FROM Spill WHERE dataQuality=0 ORDER BY spillID";

    TSQLResult* res = server->Query(query);
    int nSpills = res->GetRowCount();
    int currentRunID = ERR_VAL_G;
    for(int i = 0; i < nSpills; ++i)
    {
        if(i % 10 == 0)
        {
            cout << "Converting Spill " << i << "/" << nSpills << endl;
        }

        //basic RunID info
        TSQLRow* row = res->Next();
        event.runID = getInt(row->GetField(0));
        event.spillID = getInt(row->GetField(1));
        delete row;

        //create a new file for a new run
        if(event.runID != currentRunID)
        {
            if(currentRunID > 0)
            {
                saveFile->cd();
                saveTree->Write();
                saveFile->Close();
            }

            currentRunID = event.runID;

            cout << "Creating a new file for a new run " << currentRunID << endl;
            saveFile = new TFile(Form("%s_%06d.root", fout_dat.Data(), currentRunID), "new");
            saveTree = new TTree("save", "save");

            saveTree->Branch("Event", &p_event, 256000, 99);
        }

        //query all the events in this spill
        query = Form("SELECT b.eventID,b.MATRIX1,c.status,c.source1,c.source2,a.Intensity_p,"
            "a.`RF-16`,a.`RF-15`,a.`RF-14`,a.`RF-13`,a.`RF-12`,a.`RF-11`,a.`RF-10`,"
            "a.`RF-09`,a.`RF-08`,a.`RF-07`,a.`RF-06`,a.`RF-05`,a.`RF-04`,a.`RF-03`,"
            "a.`RF-02`,a.`RF-01`,a.`RF+00`,a.`RF+01`,a.`RF+02`,a.`RF+03`,a.`RF+04`,"
            "a.`RF+05`,a.`RF+06`,a.`RF+07`,a.`RF+08`,a.`RF+09`,a.`RF+10`,a.`RF+11`,"
            "a.`RF+12`,a.`RF+13`,a.`RF+14`,a.`RF+15`,a.`RF+16`,"
            "d.D1,d.D2,d.D3,d.H1,d.H2,d.H3,d.H4,d.P1,d.P2,d.D1L,d.D1R,d.D2L,d.D2R,d.D3L,d.D3R "
            "FROM QIE AS a,Event AS b,kEvent AS c,Occupancy AS d "
            "WHERE c.status IN (0, -10) AND a.spillID=%d "
            "a.runID=%d AND a.runID=b.runID AND c.runID = d.runID "
            "AND a.eventID=b.eventID AND a.eventID=c.eventID AND a.eventID=d.eventID ",
            event.spillID, event.runID);
        TSQLResult* res_event = server->Query(query);

        int nEvents = res_event->GetRowCount();
        for(int j = 0; j < nEvents; ++j)
        {
            TSQLRow* row_event = res_event->Next();

            event.eventID = getInt(row_event->GetField(0));
            event.MATRIX1 = getInt(row_event->GetField(1));
            event.status = getInt(row_event->GetField(2));
            event.source1 = getInt(row_event->GetField(3));
            event.source2 = getInt(row_event->GetField(4));
            event.intensityP = getFloat(row_event->GetField(5));
            for(int k = 0; k < NUM_INTENSITY; ++k) event.intensity[k] = getFloat(row_event->GetField(6+k));
            for(int k = 0; k < NUM_OCCUPANCY; ++k) event.occupancy[k] = getInt(row_event->GetField(6+NUM_INTENSITY+k));
            event.weight = 1.;

            delete row_event;

            saveTree->Fill();
        }

        delete res_event;
    }
    delete res;
    cout << "sqlEventReader finished successfully." << endl;

    saveFile->cd();
    saveTree->Write();
    saveFile->Close();

    return EXIT_SUCCESS;
}
