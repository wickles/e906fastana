#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TSQLServer.h>
#include <TSQLResult.h>
#include <TSQLRow.h>

#define GOBJECT Event
#include "sqlUtil.h"
#include "DataStruct.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 4) {
        // not enough arguments
        cout << Form("Usage: %s server schema output_file [spill_file] [sql_cut_dimuon] [sql_extra] [suffix] [ktrack_schema] [embed_source_schema|runlist] [embed_source_server] [embed_info_schema]", argv[0]) << endl;
        cout << " - For MC mode, pass 'mc' to [suffix]." << endl;
        cout << " - For extra dimuon cuts, pass 'AND [condition]' to [sql_cut_dimuon]." << endl;
        cout << " - To limit number of selected dimuons, pass 'LIMIT [num]' to [sql_extra]." << endl;
        cout << " - For messy MC, pass runlist if unmerged, or source schema and server (if different from main server)." << endl;
        cout << " - For clean MC, pass options for messy MC and schema with kEmbed (if different from kTrack schema)." << endl;
        cout << " - For embedded source data on single server, can pass C-style formatted string for [embed_source_schema], e.g. 'run_%06d_R008'." << endl;
        return -1;
    }
    // collect args
    TString server_name = argv[1];
    TString schema_main = argv[2];
    TString fout_dat = argv[3];
    TString schema_ktrack = schema_main;
    // --- optional arguments follow ---
    TString spill_file, suffix, sql_cut_dimuon, sql_extra, server_alt, schema_embed_source, schema_embed_info;
    int is_embed = 0;
    int embed_runner = 0; // type of embed: 0 (merged), 1 (unmerged, single server), 2 (unmerged, runlist for server/schema)
    map<int, run_info> runlist;
    if (argc > 4) {
        // spill file
        spill_file = argv[4];
    }
    if (argc > 5) {
        // WHERE queries, e.g. "AND runID > 100 AND runID < 200"
        sql_cut_dimuon = argv[5];
    }
    if (argc > 6) {
        // extra terminating commands, e.g. "LIMIT 10000"
        sql_extra = argv[6];
    }
    if (argc > 7) {
        // table suffix, e.g. "Mix" (modifies behavior)
        suffix = argv[7];
    }
    if (argc > 8) {
        // schema with kTracker tables
        // schema_main should contain event-level and MC tables
        schema_ktrack = argv[8];
    }
    if (argc > 9) {
        schema_embed_source = argv[9];
        schema_embed_info = schema_ktrack;
        server_alt = server_name;
        is_embed = 1;
        // if data is unmerged, pass embed schema with a C-style formatting token for the runID
        // e.g. 'run_%06d_R00X'
        if (schema_embed_source.Contains("%")) {
            embed_runner = 1;
        }
        // or pass a runlist filename
        if (schema_embed_source.EndsWith(".list") || schema_embed_source.EndsWith(".txt")) {
            embed_runner = 2;
            get_runlist(runlist, schema_embed_source);
        }
    }
    if (argc > 10) {
        server_alt = argv[10];
    }
    if (argc > 11) {
        schema_embed_info = argv[11];
        // set to 2 to indicate clean MC
        is_embed = 2;
    }

    TFile* saveFile = new TFile(fout_dat, "new");
    TTree* saveTree = new TTree("save", "save");
    if (saveFile->IsZombie()) return -1;

    // define the output file structure
    Dimuon* p_dimuon = new Dimuon; Dimuon& dimuon = *p_dimuon;
    Spill* p_spill = new Spill; Spill& spill = *p_spill;
    Event* p_event = new Event; Event& event = *p_event;
    Track* p_posTrack = new Track; Track& posTrack = *p_posTrack;
    Track* p_negTrack = new Track; Track& negTrack = *p_negTrack;
    Dimuon* p_mc_dimuon = new Dimuon; Dimuon& mc_dimuon = *p_mc_dimuon;
    // TODO: mTracks...
    //Track* p_mc_posTrack = new Track; Track& mc_posTrack = *p_mc_posTrack;
    //Track* p_mc_negTrack = new Track; Track& mc_negTrack = *p_mc_negTrack;

    g906 = p_event;

    saveTree->Branch("dimuon", &p_dimuon, 256000, 99);
    saveTree->Branch("event", &p_event, 256000, 99);
    saveTree->Branch("spill", &p_spill, 256000, 99);
    saveTree->Branch("posTrack", &p_posTrack, 256000, 99);
    saveTree->Branch("negTrack", &p_negTrack, 256000, 99);
    saveTree->Branch("mc_dimuon", &p_mc_dimuon, 256000, 99);

    //Connect to server
    TSQLServer* server = TSQLServer::Connect(Form("mysql://%s", server_name.Data()), seaquel_user, seaquel_passwd);
    server->Exec(Form("USE %s", schema_main.Data()));
    cout << "Reading from schema: " << schema_main << endl;
    cout << "Writing to file: " << fout_dat << endl;

    //Connect to alt server if necessary
    TSQLServer* srv_alt = NULL;
    if (is_embed && embed_runner == 1) {
        srv_alt = TSQLServer::Connect(Form("mysql://%s", server_alt.Data()), seaquel_user, seaquel_passwd);
    }

    // store connections for embed servers .. TODO: generalize to all connections used
    sql_connector connector;

    //decide how to access event/spill level information
    spill.skipflag = suffix.Contains("Mix", TString::kIgnoreCase);
    // if suffix arg contains "mc", set spill skipflag and mcflag
    bool mcflag = false;
    if (suffix.Contains("mc", TString::kIgnoreCase)) {
        mcflag = true;
        suffix = "";
        spill.skipflag = true;
        // query and print some info
        TSQLResult* res_info = server->Query(Form("SELECT * FROM %s.kInfo "
                    "WHERE infoKey IN ('version', 'kTrackerVer', 'Geometry', 'Realization')", schema_ktrack.Data()));
        TSQLRow* row_info = NULL;
        cout << " -- MC info -- " << endl;
        for (int i = 0; i < res_info->GetRowCount(); ++i) {
            row_info = res_info->Next();
            cout << row_info->GetField(0) << " : " << row_info->GetField(1) << endl;
        }
        delete row_info;
        delete res_info;
    }

    //pre-load spill information if provided
    map<int, Spill> spillBank;
    if(!spill.skipflag)
    {
        TFile* spillFile = new TFile(spill_file);
        TTree* spillTree = (TTree*)spillFile->Get("save");
        spillTree->SetBranchAddress("spill", &p_spill);

        for(int i = 0; i < spillTree->GetEntries(); ++i)
        {
            spillTree->GetEntry(i);
            if(spill.goodSpill())  spillBank.insert(map<int, Spill>::value_type(spill.spillID, spill));
        }
    }

    TString query = Form("SELECT dimuonID,runID,spillID,eventID,posTrackID,negTrackID,dx,dy,dz,"
        "dpx,dpy,dpz,mass,xF,xB,xT,trackSeparation,chisq_dimuon,px1,py1,pz1,px2,py2,pz2,costh,phi,targetPos "
        "FROM %s.kDimuon%s WHERE mass>0.5 AND mass<10. AND chisq_dimuon<50. AND xB>0. AND xB<1. AND "
        "xT>0. AND xT<1. AND xF>-1. AND xF<1. AND ABS(dx)<2. AND ABS(dy-1)<3. AND dz>-300. "
        "AND dz<300. AND ABS(dpx)<3. AND ABS(dpy)<3. AND dpz>30. AND dpz<120. AND "
        "ABS(trackSeparation)<300. %s ORDER BY spillID %s",
        schema_ktrack.Data(), suffix.Data(), sql_cut_dimuon.Data(), sql_extra.Data());

    cout << " -- Query for dimuons: " << endl << query << endl;
    TSQLResult* res_dimuon = server->Query(query);
    int nDimuonsRaw = res_dimuon->GetRowCount();
    cout << " -- Query completed, total dimuons: " << nDimuonsRaw << endl;

    long long skipped = 0;
    long long bad_queries = 0;
    long long bad_spills = 0;
    spill.spillID = -1;
    bool badSpillFlag = false;
    for(int i = 0; i < nDimuonsRaw; ++i)
    {
        if(i % 1000 == 0)
        {
            cout << "Converting dimuon " << i << "/" << nDimuonsRaw << endl;
        }

        //basic dimuon info
        TSQLRow* row_dimuon = res_dimuon->Next();

        dimuon.dimuonID     = getInt(row_dimuon->GetField(0));
        event.runID         = getInt(row_dimuon->GetField(1));
        event.spillID       = getInt(row_dimuon->GetField(2));
        event.eventID       = getInt(row_dimuon->GetField(3));
        dimuon.posTrackID   = getInt(row_dimuon->GetField(4));
        dimuon.negTrackID   = getInt(row_dimuon->GetField(5));
        dimuon.dx           = getFloat(row_dimuon->GetField(6));
        dimuon.dy           = getFloat(row_dimuon->GetField(7));
        dimuon.dz           = getFloat(row_dimuon->GetField(8));
        dimuon.dpx          = getFloat(row_dimuon->GetField(9));
        dimuon.dpy          = getFloat(row_dimuon->GetField(10));
        dimuon.dpz          = getFloat(row_dimuon->GetField(11));
        dimuon.mass         = getFloat(row_dimuon->GetField(12));
        dimuon.xF           = getFloat(row_dimuon->GetField(13));
        dimuon.x1           = getFloat(row_dimuon->GetField(14));
        dimuon.x2           = getFloat(row_dimuon->GetField(15));
        dimuon.trackSeparation = getFloat(row_dimuon->GetField(16));
        dimuon.chisq_dimuon = getFloat(row_dimuon->GetField(17));
        dimuon.px1          = getFloat(row_dimuon->GetField(18));
        dimuon.py1          = getFloat(row_dimuon->GetField(19));
        dimuon.pz1          = getFloat(row_dimuon->GetField(20));
        dimuon.px2          = getFloat(row_dimuon->GetField(21));
        dimuon.py2          = getFloat(row_dimuon->GetField(22));
        dimuon.pz2          = getFloat(row_dimuon->GetField(23));
        dimuon.costh        = getFloat(row_dimuon->GetField(24));
        dimuon.phi          = getFloat(row_dimuon->GetField(25));
        dimuon.pT           = sqrt(dimuon.dpx*dimuon.dpx + dimuon.dpy*dimuon.dpy);
        int targetPos       = getInt(row_dimuon->GetField(26));

        delete row_dimuon;

        // TODO: clean this up for clarity
        //spill info and cuts
        if (spill.skipflag) // MC or Mix
        {
            // set necessary values for Mix/MC
            spill.runID = event.runID;
            spill.spillID = event.spillID;
            spill.targetPos = targetPos;
            spill.TARGPOS_CONTROL = targetPos;
        }
        else if(event.spillID != spill.spillID) // Real data, new spill
        {
            // NOTE: for previously loaded spills, spill data should already be loaded (is this intended ROOT behavior?)
            event.log("New spill!");
            spill.spillID = event.spillID;

            //check if the spill exists in pre-loaded spills
            if(spillBank.find(event.spillID) == spillBank.end())
            {
                event.log("spill does not exist!");
                badSpillFlag = true;
            }
            else
            {
                spill = spillBank[event.spillID];
                badSpillFlag = false;
            }
        }
        if(!badSpillFlag && (targetPos != spill.TARGPOS_CONTROL || !spill.goodTargetPos())) {
            event.log(Form("ERROR: bad target (kDimuon%s.targetPos = %d)", suffix.Data(), targetPos));
            badSpillFlag = true;
        }
        if(badSpillFlag) {
            bad_spills += 1;
            continue;
        }


        // write default/error values for missable quantities
        event.weight = 0;
        event.source1 = -1;
        event.source2 = -1;
        event.passed = -1;
        event.Ntr = -1;
        event.Ndi = -1;
        event.intensityP = ERR_VAL_G;
        for(int j = 0; j < NUM_INTENSITY; ++j) event.intensity[j] = ERR_VAL_G;
        for(int j = 0; j < NUM_OCCUPANCY; ++j) event.occupancy[j] = ERR_VAL_G;
        for(int j = 0; j <= NUM_TRIGGER; ++j) event.MATRIX[j] = ERR_VAL_G;
        for(int j = 0; j <= NUM_TRIGGER; ++j) event.NIM[j] = ERR_VAL_G;


        // get event info, QIE, occupancy
        TSQLResult* res_event;
        TSQLRow* row_event;
        // TODO: consolidate duplicated code (status/source/matrix1)
        if(!spill.skipflag) // real data
        {
            // query for tracker multiplicities
            // TODO: Apply also to MC. how to apply to Mix?
            query = Form("SELECT "
                "(SELECT COUNT(*) FROM %s.kTrack%s WHERE eventID=%d),"
                "(SELECT COUNT(*) FROM %s.kDimuon%s WHERE eventID=%d)",
            schema_ktrack.Data(), suffix.Data(), event.eventID, schema_ktrack.Data(), suffix.Data(), event.eventID);
            res_event = server->Query(query);
            if(res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for event multiplicities.");
                delete res_event;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.Ntr = getInt(row_event->GetField(0));
                event.Ndi = getInt(row_event->GetField(1));
                delete row_event;
                delete res_event;
            }


            // TODO: remove sources if always -1?
            query = Form("SELECT kE.status,kE.source1,kE.source2, "
                "E.MATRIX1,E.MATRIX2,E.MATRIX3,E.MATRIX4,E.MATRIX5, "
                "E.NIM1,E.NIM2,E.NIM3,E.NIM4,E.NIM5 "
                "FROM %s.kEvent AS kE, %s.Event AS E  "
                "WHERE kE.runID=%d AND kE.eventID=%d AND E.runID=kE.runID AND E.eventID=kE.eventID",
                schema_ktrack.Data(), schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if(res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for basic event-level data.");
                delete res_event;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.status = getInt(row_event->GetField(0));
                event.source1 = getInt(row_event->GetField(1));
                event.source2 = getInt(row_event->GetField(2));
                //event.MATRIX = getInt(row_event->GetField(3));
                for(int j = 1; j <= NUM_TRIGGER; ++j) event.MATRIX[j] = getInt(row_event->GetField(2+j), ERR_VAL_G);
                for(int j = 1; j <= NUM_TRIGGER; ++j) event.NIM[j] = getInt(row_event->GetField(2+NUM_TRIGGER+j), ERR_VAL_G);
                event.weight = 1;
                delete row_event;
                delete res_event;
            }

            // query for basic QIE data
            query = Form("SELECT "
                "q.`RF-16`,q.`RF-15`,q.`RF-14`,q.`RF-13`,q.`RF-12`,q.`RF-11`,q.`RF-10`,"
                "q.`RF-09`,q.`RF-08`,q.`RF-07`,q.`RF-06`,q.`RF-05`,q.`RF-04`,q.`RF-03`,"
                "q.`RF-02`,q.`RF-01`,q.`RF+00`,q.`RF+01`,q.`RF+02`,q.`RF+03`,q.`RF+04`,"
                "q.`RF+05`,q.`RF+06`,q.`RF+07`,q.`RF+08`,q.`RF+09`,q.`RF+10`,q.`RF+11`,"
                "q.`RF+12`,q.`RF+13`,q.`RF+14`,q.`RF+15`,q.`RF+16` "
                "FROM %s.QIE AS q WHERE q.runID=%d AND q.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for QIE intensities RF-XX.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < NUM_INTENSITY; ++j) event.intensity[j] = getFloat(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }


            // query for basic occupancy data
            query = Form("SELECT o.D1,o.D2,o.D3,o.H1,o.H2,o.H3,o.H4,o.P1,o.P2 "
                "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for basic occupancies.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < 9; ++j) event.occupancy[j] = getInt(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // query for additional QIE data
            query = Form("SELECT q.Intensity_p, q.PotPerQie FROM %s.QIE AS q WHERE q.runID=%d AND q.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                // TODO: get spill-level data from embedded event in case that intensity_p was not precomputed?
                //event.intensityP = event.weightedIntensity(spill.QIEUnit(), spill.pedestal());
                event.log("ERROR: Query failed for QIE field: Intensity_p.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.intensityP = getFloat(row_event->GetField(0), ERR_VAL_G);
                event.PotPerQie = getFloat(row_event->GetField(1), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // query for additional occupancy data
            query = Form("SELECT o.D1L,o.D1R,o.D2L,o.D2R,o.D3L,o.D3R "
                "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for L/R occupancies D[1-3][LR].");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < NUM_OCCUPANCY-9; ++j) event.occupancy[9+j] = getInt(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }
        }
        else if (mcflag) // MC
        {
            // TODO: remove sources if always -1?
            // TODO: get MATRIX1 even for MC? (is this useful?)
            query = Form("SELECT kE.status,kE.source1,kE.source2,mD.sigWeight "
                "FROM %s.kEvent AS kE, %s.mDimuon AS mD "
                "WHERE kE.runID=%d AND kE.eventID=%d AND mD.runID=kE.runID AND mD.eventID=kE.eventID",
                schema_ktrack.Data(), schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query.Data());
            row_event = res_event->Next();
            event.MATRIX[1] = 1;
            event.status = getInt(row_event->GetField(0));
            event.source1 = getInt(row_event->GetField(1));
            event.source2 = getInt(row_event->GetField(2));
            event.weight = getFloat(row_event->GetField(3));
            delete row_event;
            delete res_event;

#if 0
            // in principle, this will get the intrinsic MC occupancy
            // but should not be used, because they will basically always be 6, 12, or 18
            // so not very useful.
            query = Form("SELECT "
                "SUM( CASE WHEN detectorName LIKE 'D1%' THEN 1 ELSE 0 END ) AS D1,"
                "SUM( CASE WHEN detectorName LIKE 'D2%' THEN 1 ELSE 0 END ) AS D2,"
                "SUM( CASE WHEN detectorName LIKE 'D3%' THEN 1 ELSE 0 END ) AS D3,"
                "SUM( CASE WHEN detectorName LIKE 'H1%' THEN 1 ELSE 0 END ) AS H1,"
                "SUM( CASE WHEN detectorName LIKE 'H2%' THEN 1 ELSE 0 END ) AS H2,"
                "SUM( CASE WHEN detectorName LIKE 'H3%' THEN 1 ELSE 0 END ) AS H3,"
                "SUM( CASE WHEN detectorName LIKE 'H4%' THEN 1 ELSE 0 END ) AS H4,"
                "SUM( CASE WHEN detectorName LIKE 'P1%' THEN 1 ELSE 0 END ) AS P1,"
                "SUM( CASE WHEN detectorName LIKE 'P2%' THEN 1 ELSE 0 END ) AS P2 "
                "FROM %s.Hit WHERE runID=%d AND eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            row_event = res_event->Next();
            for(int j = 0; j < 9; ++j) {
                event.occupancy[j] = getInt(row_event->GetField(j), ERR_VAL_G);
            }
            delete row_event;
            delete res_event;
#endif

            // NOTE: Clean MC doesn't really have occupancy, but messy embeds should be collected to be used as a proxy for computing efficiency.
            // NOTE: Sometimes messy/clean can appear inconsistent as pass/total after cuts, since messy/clean ktracked values can differ somewhat.
            // So use 'passed' flag determined for clean MC to ensure consistency.

            // Clean MC, determine passed based on successful messy reconstruction (messy kDimuon exists)
            if (is_embed == 2) {
                query = Form("SELECT eventID FROM %s.kDimuon%s WHERE eventID=%d", schema_embed_info.Data(), suffix.Data(), event.eventID);
                res_event = server->Query(query);
                if (res_event == NULL) {
                    event.log("Query failed for messy/clean pass determination..");
                }
                else if (res_event->GetRowCount() <= 0) {
                    // not found in messy, so not passed
                    event.passed = 0;
                }
                else {
                    event.passed = 1;
                }
                delete res_event;
            }

            // Get embedded MC source data
            // kEmbed table available since ~M017. Key values: runID, eventID
            // Maybe get QIE/Occ data from ROOT data file instead of SQL?
            if (is_embed) {
                // query main schema, save embed runID as source1, embed eventID as source2
                query = Form("SELECT eRunID, eEventID FROM %s.kEmbed WHERE runID=%d AND eventID=%d",
                        schema_embed_info.Data(), event.runID, event.eventID);
                res_event = server->Query(query);
                if (res_event == NULL || res_event->GetRowCount() <= 0) {
                    bad_queries += 1;
                    event.log(Form("ERROR: Bad query for embedding info | bad_queries -> %d", bad_queries));
                    delete res_event;
                }
                else {
                    do { // do..while(0) loop for easy exiting upon errors
                        row_event = res_event->Next();
                        event.source1 = getInt(row_event->GetField(0));
                        event.source2 = getInt(row_event->GetField(1));
                        delete row_event;
                        delete res_event;

                        // NOTE: no need to include embedded eventID in error log, already included as source[1-2]
                        int e_runID = event.source1;
                        int e_eventID = event.source2;
                        if (e_runID <= 0 || e_eventID <= 0) {
                            event.log("ERROR: Invalid embedded event.");
                            continue;
                        }

                        // Determine the occupancy schema to search
                        TString schema_run = schema_embed_source;
                        TSQLServer * srv_run = srv_alt;
                        if (embed_runner == 1) {
                            // Form run schema from C-formatted input string
                            schema_run = Form(schema_embed_source, e_runID);
                        }
                        if (embed_runner == 2) {
                            schema_run = runlist[e_runID].schema;
                            TString srv_name = Form("mysql://%s", expand_server(runlist[e_runID].server.Data()).c_str());
                            srv_run = connector.getcon(srv_name);
                            if (srv_run == NULL) {
                                event.log(Form("ERROR: Null connection to: %s/%s",
                                    runlist[e_runID].server.Data(), runlist[e_runID].schema.Data()));
                                continue;
                            }
                        }

                        // Query for various intensity and occupancy fields.
                        // Make sure to do queries separately
                        // and do error checking to avoid crashing on missing/null tables or fields

                        // query embed source for basic QIE data
                        query = Form("SELECT "
                            "q.`RF-16`,q.`RF-15`,q.`RF-14`,q.`RF-13`,q.`RF-12`,q.`RF-11`,q.`RF-10`,"
                            "q.`RF-09`,q.`RF-08`,q.`RF-07`,q.`RF-06`,q.`RF-05`,q.`RF-04`,q.`RF-03`,"
                            "q.`RF-02`,q.`RF-01`,q.`RF+00`,q.`RF+01`,q.`RF+02`,q.`RF+03`,q.`RF+04`,"
                            "q.`RF+05`,q.`RF+06`,q.`RF+07`,q.`RF+08`,q.`RF+09`,q.`RF+10`,q.`RF+11`,"
                            "q.`RF+12`,q.`RF+13`,q.`RF+14`,q.`RF+15`,q.`RF+16` "
                            "FROM %s.QIE AS q WHERE q.runID=%d AND q.eventID=%d",
                            schema_run.Data(), e_runID, e_eventID);
                        res_event = srv_run->Query(query);
                        if (res_event == NULL || res_event->GetRowCount() != 1) {
                            event.log("ERROR: Query failed for embedded QIE intensities RF-XX.");
                            delete res_event;
                            bad_queries += 1;
                            //continue;
                        }
                        else {
                            row_event = res_event->Next();
                            for(int j = 0; j < NUM_INTENSITY; ++j) event.intensity[j] = getFloat(row_event->GetField(j), ERR_VAL_G);
                            delete row_event;
                            delete res_event;
                        }


                        // query embed source for basic occupancy data
                        query = Form("SELECT o.D1,o.D2,o.D3,o.H1,o.H2,o.H3,o.H4,o.P1,o.P2 "
                            "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID=%d",
                            schema_run.Data(), e_runID, e_eventID);
                        res_event = srv_run->Query(query);
                        if (res_event == NULL || res_event->GetRowCount() != 1) {
                            event.log("ERROR: Query failed for embedded basic occupancies.");
                            delete res_event;
                            bad_queries += 1;
                            //continue;
                        }
                        else {
                            row_event = res_event->Next();
                            for(int j = 0; j < 9; ++j) event.occupancy[j] = getInt(row_event->GetField(j), ERR_VAL_G);
                            delete row_event;
                            delete res_event;
                        }

                        // query embed source for additional QIE data
                        query = Form("SELECT q.Intensity_p FROM %s.QIE AS q WHERE q.runID=%d AND q.eventID=%d",
                            schema_run.Data(), e_runID, e_eventID);
                        res_event = srv_run->Query(query);
                        if (res_event == NULL || res_event->GetRowCount() != 1) {
                            // TODO: get spill-level data from embedded event in case that intensity_p was not precomputed?
                            //event.intensityP = event.weightedIntensity(spill.QIEUnit(), spill.pedestal());
                            event.log("ERROR: Query failed for embedded QIE field: Intensity_p.");
                            delete res_event;
                            bad_queries += 1;
                            //continue;
                        }
                        else {
                            row_event = res_event->Next();
                            event.intensityP = getFloat(row_event->GetField(0), ERR_VAL_G);
                            delete row_event;
                            delete res_event;
                        }

                        // query embed source for additional occupancy data
                        query = Form("SELECT o.D1L,o.D1R,o.D2L,o.D2R,o.D3L,o.D3R "
                            "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID=%d",
                            schema_run.Data(), e_runID, e_eventID);
                        res_event = srv_run->Query(query);
                        if (res_event == NULL || res_event->GetRowCount() != 1) {
                            event.log("ERROR: Query failed for embedded L/R occupancies D[1-3][LR].");
                            delete res_event;
                            bad_queries += 1;
                            //continue;
                        }
                        else {
                            row_event = res_event->Next();
                            for(int j = 0; j < NUM_OCCUPANCY-9; ++j) event.occupancy[9+j] = getInt(row_event->GetField(j), ERR_VAL_G);
                            delete row_event;
                            delete res_event;
                        }

                    } while(0);
                }
            }

#if 1
            // get thrown MC values
            query = Form("SELECT mTrackID1,mTrackID2,dx,dy,dz,"
                    "dpx,dpy,dpz,mass,xF,xB,xT,COS(theta_mu),phi_mu "
                    "FROM %s.mDimuon%s WHERE eventID=%d",
                    schema_main.Data(), suffix.Data(), event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Bad query for mDimuon data");
                delete res_event;
            }
            else {
                row_event = res_event->Next();
                mc_dimuon.dimuonID     = -1;
                mc_dimuon.posTrackID   = getInt(row_event->GetField(0));
                mc_dimuon.negTrackID   = getInt(row_event->GetField(1));
                mc_dimuon.dx           = getFloat(row_event->GetField(2));
                mc_dimuon.dy           = getFloat(row_event->GetField(3));
                mc_dimuon.dz           = getFloat(row_event->GetField(4));
                mc_dimuon.dpx          = getFloat(row_event->GetField(5));
                mc_dimuon.dpy          = getFloat(row_event->GetField(6));
                mc_dimuon.dpz          = getFloat(row_event->GetField(7));
                mc_dimuon.mass         = getFloat(row_event->GetField(8));
                mc_dimuon.xF           = getFloat(row_event->GetField(9));
                mc_dimuon.x1           = getFloat(row_event->GetField(10));
                mc_dimuon.x2           = getFloat(row_event->GetField(11));
                mc_dimuon.trackSeparation = -1;
                mc_dimuon.chisq_dimuon = -1;
                mc_dimuon.px1          = -1;
                mc_dimuon.py1          = -1;
                mc_dimuon.pz1          = -1;
                mc_dimuon.px2          = -1;
                mc_dimuon.py2          = -1;
                mc_dimuon.pz2          = -1;
                mc_dimuon.costh        = getFloat(row_event->GetField(12));
                mc_dimuon.phi          = getFloat(row_event->GetField(13));
                mc_dimuon.pT           = sqrt(mc_dimuon.dpx*mc_dimuon.dpx + mc_dimuon.dpy*mc_dimuon.dpy);
                delete row_event;
                delete res_event;
            }
            // TODO: mTrack data..
#endif
        }
        else // Mix
        {
            query = Form("SELECT kE.status,kE.source1,kE.source2 FROM %s.kEvent%s AS kE "
                "WHERE kE.runID=%d AND kE.eventID=%d", schema_ktrack.Data(), suffix.Data(), event.runID, event.eventID);
            res_event = server->Query(query.Data());
            row_event = res_event->Next();
            event.MATRIX[1] = 1;
            event.status = getInt(row_event->GetField(0));
            event.source1 = getInt(row_event->GetField(1));
            event.source2 = getInt(row_event->GetField(2));
            event.weight = 1.;

            delete row_event;
            delete res_event;

            // Mix QIE: average over intensities
            query = Form("SELECT AVG(q.`RF-16`),AVG(q.`RF-15`),AVG(q.`RF-14`),"
                "AVG(q.`RF-13`),AVG(q.`RF-12`),AVG(q.`RF-11`),AVG(q.`RF-10`),AVG(q.`RF-09`),"
                "AVG(q.`RF-08`),AVG(q.`RF-07`),AVG(q.`RF-06`),AVG(q.`RF-05`),AVG(q.`RF-04`),"
                "AVG(q.`RF-03`),AVG(q.`RF-02`),AVG(q.`RF-01`),AVG(q.`RF+00`),AVG(q.`RF+01`),"
                "AVG(q.`RF+02`),AVG(q.`RF+03`),AVG(q.`RF+04`),AVG(q.`RF+05`),AVG(q.`RF+06`),"
                "AVG(q.`RF+07`),AVG(q.`RF+08`),AVG(q.`RF+09`),AVG(q.`RF+10`),AVG(q.`RF+11`),"
                "AVG(q.`RF+12`),AVG(q.`RF+13`),AVG(q.`RF+14`),AVG(q.`RF+15`),AVG(q.`RF+16`) "
                "FROM %s.QIE q WHERE q.runID=%d AND q.eventID IN (%d, %d)",
                schema_main.Data(), event.runID, event.source1, event.source2);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for mixed QIE intensities RF-XX.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < NUM_INTENSITY; ++j) event.intensity[j] = getFloat(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // Mix Occupancy: average over occupancies
            query = Form("SELECT AVG(o.D1),AVG(o.D2),AVG(o.D3),AVG(o.H1),AVG(o.H2),AVG(o.H3),AVG(o.H4),AVG(o.P1),AVG(o.P2) "
                "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID IN (%d, %d)",
                schema_main.Data(), event.runID, event.source1, event.source2);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for mixed basic occupancies.");
                delete res_event;
                bad_queries += 1;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < 9; ++j) event.occupancy[j] = (int)getFloat(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // Mix intensityP: average intensityP
            query = Form("SELECT AVG(Intensity_p) FROM %s.QIE WHERE runID=%d AND eventID IN (%d, %d)",
                schema_main.Data(), event.runID, event.source1, event.source2);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                // TODO: compute from spill-level data?
                //event.intensityP = event.weightedIntensity(spill.QIEUnit(), spill.pedestal());
                event.log("ERROR: Query failed for mixed QIE field: Intensity_p.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.intensityP = getFloat(row_event->GetField(0), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // Mix L/R Occ: D1L = pos.D1L+neg.D1R, D1R = pos.D1R+neg.D1L, ... etc
            query = Form("SELECT o1.D1L+o2.D1R, o1.D1R+o2.D1L, o1.D2L+o2.D2R, o1.D2R+o2.D2L, o1.D3L+o2.D3R, o1.D3R+o2.D3L "
                    "FROM %s.Occupancy AS o1, %s.Occupancy AS o2 WHERE o1.eventID=%d AND o2.eventID=%d",
                    schema_main.Data(), schema_main.Data(), event.source1, event.source2);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for mixed L/R occupancies D[1-3][LR].");
                delete res_event;
                bad_queries += 1;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < NUM_OCCUPANCY-9; ++j) event.occupancy[9+j] = getInt(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }
        }

        //track info
        query = Form("SELECT numHits,numHitsSt1,numHitsSt2,numHitsSt3,numHitsSt4H,numHitsSt4V,"
            "chisq,chisq_target,chisq_dump,chisq_upstream,x1,y1,z1,x3,y3,z3,x0,y0,z0,xT,yT,"
            "xD,yD,px1,py1,pz1,px3,py3,pz3,px0,py0,pz0,pxT,pyT,pzT,pxD,pyD,pzD,roadID,"
            "tx_PT,ty_PT,thbend,kmstatus,z0x,z0y FROM %s.kTrack%s "
            "WHERE runID=%d AND eventID=%d AND trackID IN (%d,%d) ORDER BY charge DESC",
            schema_ktrack.Data(), suffix.Data(), event.runID, event.eventID, dimuon.posTrackID, dimuon.negTrackID);

        TSQLResult* res_track = server->Query(query);
        if(res_track == NULL || res_track->GetRowCount() != 2)
        {
            event.log("ERROR: Query failed for kTracks.");
            delete res_track;
            continue;
            skipped += 1;
        }

        // record data for both tracks
        for(int j=0; j<2; j++) {
            TSQLRow* row_track = res_track->Next();
            Track& track = (j==0 ? posTrack : negTrack);

            if (j==0) {
                track.trackID = dimuon.posTrackID;
                track.charge  = 1;
                track.pxv     = dimuon.px1;
                track.pyv     = dimuon.py1;
                track.pzv     = dimuon.pz1;
            }
            else {
                track.trackID = dimuon.negTrackID;
                track.charge  = -1;
                track.pxv     = dimuon.px2;
                track.pyv     = dimuon.py2;
                track.pzv     = dimuon.pz2;
            }

            track.nHits     = getInt(row_track->GetField(0));
            track.nHitsSt1  = getInt(row_track->GetField(1));
            track.nHitsSt2  = getInt(row_track->GetField(2));
            track.nHitsSt3  = getInt(row_track->GetField(3));
            track.nHitsSt4H = getInt(row_track->GetField(4));
            track.nHitsSt4V = getInt(row_track->GetField(5));
            track.chisq          = getFloat(row_track->GetField(6));
            track.chisq_target   = getFloat(row_track->GetField(7));
            track.chisq_dump     = getFloat(row_track->GetField(8));
            track.chisq_upstream = getFloat(row_track->GetField(9));
            track.x1    = getFloat(row_track->GetField(10));
            track.y1    = getFloat(row_track->GetField(11));
            track.z1    = getFloat(row_track->GetField(12));
            track.x3    = getFloat(row_track->GetField(13));
            track.y3    = getFloat(row_track->GetField(14));
            track.z3    = getFloat(row_track->GetField(15));
            track.x0    = getFloat(row_track->GetField(16));
            track.y0    = getFloat(row_track->GetField(17));
            track.z0    = getFloat(row_track->GetField(18));
            track.xT    = getFloat(row_track->GetField(19));
            track.yT    = getFloat(row_track->GetField(20));
            track.zT    = -129.;
            track.xD    = getFloat(row_track->GetField(21));
            track.yD    = getFloat(row_track->GetField(22));
            track.zD    = 42.;
            track.px1   = getFloat(row_track->GetField(23));
            track.py1   = getFloat(row_track->GetField(24));
            track.pz1   = getFloat(row_track->GetField(25));
            track.px3   = getFloat(row_track->GetField(26));
            track.py3   = getFloat(row_track->GetField(27));
            track.pz3   = getFloat(row_track->GetField(28));
            track.px0   = getFloat(row_track->GetField(29));
            track.py0   = getFloat(row_track->GetField(30));
            track.pz0   = getFloat(row_track->GetField(31));
            track.pxT   = getFloat(row_track->GetField(32));
            track.pyT   = getFloat(row_track->GetField(33));
            track.pzT   = getFloat(row_track->GetField(34));
            track.pxD   = getFloat(row_track->GetField(35));
            track.pyD   = getFloat(row_track->GetField(36));
            track.pzD   = getFloat(row_track->GetField(37));
            track.roadID = getInt(row_track->GetField(38));
            track.tx_PT  = getFloat(row_track->GetField(39));
            track.ty_PT  = getFloat(row_track->GetField(40));
            track.thbend = getFloat(row_track->GetField(41));
            track.kmstatus = getInt(row_track->GetField(42));
            track.z0x    = getInt(row_track->GetField(43));
            track.z0y    = getInt(row_track->GetField(44));

            delete row_track;
        }

        delete res_track;

        //Now save everything
        saveTree->Fill();
        if(saveTree->GetEntries() % 1000 == 0) saveTree->AutoSave("self");
    }
    delete res_dimuon;

    cout << Form("sqlReader finished reading %s tables successfully.", suffix.Data()) << endl;
    cout << "Bad spills: " << bad_spills << endl;
    cout << "Bad queries: " << bad_queries << endl;
    cout << "Skipped: " << skipped << endl;

    saveFile->cd();
    saveTree->Write();
    saveFile->Close();

    return EXIT_SUCCESS;
}
