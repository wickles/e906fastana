#!/usr/bin/python
# Run sqlDataReader for individual runs
# Mainly for R007+ real data where there is no merged schema

import sys
import os


# Helper functions

HOST_E906_2 = "e906-db2.fnal.gov"
HOST_E906_3 = "e906-db3.fnal.gov"
HOST_SEAQUEST_1 = "seaquestdb01.fnal.gov"
HOST_UIUC = "seaquel.physics.illinois.edu"
PORT_SEAQUEST_1 = 3310
PORT_UIUC = 3283
SRV_E906_2 = HOST_E906_2
SRV_E906_3 = HOST_E906_2
SRV_SEAQUEST_1 = HOST_SEAQUEST_1 + ":" + repr(PORT_SEAQUEST_1)
SRV_UIUC = HOST_UIUC + ":" + repr(PORT_UIUC)

def expand_server(server):
    if server == "db2":
        server = HOST_E906_2
    if server == "db3":
        server = HOST_E906_3
    if server == "seaquel" or server == "uiuc":
        server = HOST_UIUC
    if server == "seaquestdb01" or server == HOST_E906_2:
        server = HOST_SEAQUEST_1
    if server == HOST_UIUC:
        server = SRV_UIUC
    if server == HOST_SEAQUEST_1:
        server = SRV_SEAQUEST_1
    return server

# Configure parameters

out_dir = "tmp"
spill_dir = ""
sql_cut = ""
suffix = ""
do_spills = True
do_singles = False
#run_min = 0
#run_num = 10e6
if (len(sys.argv) > 1):
    runlistfile = sys.argv[1]
    if (len(sys.argv) > 2):
        out_dir = sys.argv[2]
    if (len(sys.argv) > 3):
        spill_dir = sys.argv[3]
        do_spills = False
    if (len(sys.argv) > 4):
        sql_cut = sys.argv[4]
        if (sql_cut == 'SINGLES'):
            do_singles = True
            sql_cut = ''
    if (len(sys.argv) > 5):
        suffix = sys.argv[5]
    #run_num = ...
    #run_min = ...
else:
    # help prompt
    print "Syntax: %s runlistfile [out_dir] [spill_dir] [sql_cut_dimuon] [suffix]" % sys.argv[0]
    print "To make spill files, only pass 'out_dir'"
    print "To query singles, pass sql_cut_dimuon as 'SINGLES'"
    quit()

#if (run_min < 0 or run_num <= 0):
#    print "run_start must be non-negative, run_count must be positive"
#    quit()

# Main function

#print "Attempting query, writing results to file: " + results_file
run_count = 0
bad_runs = []

with open(runlistfile) as f:
    for line in f:
        #if (run_count >= run_num):
        #    break

        spl = line.split()
        if (len(spl) < 3):
            print "Bad line: " + line
            continue
        server, schema, extra = spl[0], spl[1], spl[2]
        schema_ktrack = schema
        if (len(spl) >= 4):
            schema_ktrack = spl[3]
        #run = int(schema.split('_')[1])
        server = expand_server(server)

        #if (run >= run_min):
        port = 3306
        if (server == "seaquel.physics.illinois.edu"):
            port = 3283
        if (server == "seaquestdb01.fnal.gov"):
            port = 3310
        if do_spills:
            os.system("./sqlSpillReader '{0}:{1}' '{2}' '{3}/spill_{4}.root' '{4}'".format(server, port, schema, out_dir, schema_ktrack))
        elif do_singles:
            os.system("./sqlSinglesReader '{0}:{1}' '{2}' '{3}/singles_{7}.root' '{4}/spill_{7}.root' '{5}' '' '{6}' '{7}'".format(server, port, schema, out_dir, spill_dir, sql_cut, suffix, schema_ktrack))
        else:
            os.system("./sqlDataReader '{0}:{1}' '{2}' '{3}/data_{7}.root' '{4}/spill_{7}.root' '{5}' '' '{6}' '{7}'".format(server, port, schema, out_dir, spill_dir, sql_cut, suffix, schema_ktrack))


if (len(bad_runs) > 0):
    print "Bad/empty runs: " + repr(bad_runs)

