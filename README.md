# e906fastana

Fast analysis of e906 final results

If issues occur upon running code depending on the `libAnaUtil.so` library, e.g.
- `error while loading shared libraries: libAnaUtil.so: cannot open shared object file: No such file or directory`
- `symbol lookup error: .../e906fastana/libAnaUtil.so: undefined symbol: _ZN4ROOT14RegisterModuleEv`
then try running `setup.sh` (sets LD path), and `$ROOTSYS/bin/thisroot.sh` (does important rooty things).


# Notes

Major/breaking changes in R007+
- Beam offset is accounted for by kTracker. This affects certain variables' distributions: dimuon.dy, ... Be careful when making cuts! (Use offset var $YOFFS)
- Runs are not merged on SQL servers. Use `sqlRunner.py` to read data for individual runs, then merge using ROOT's `hadd` function.
