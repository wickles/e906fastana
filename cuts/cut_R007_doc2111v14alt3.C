// physics cuts
// 4 < dimuon.mass && dimuon.mass < 8.5 && 
// -0.2 < dimuon.xF && dimuon.xF < 0.9 && 
// 0.07 < dimuon.x2 && 
abs(dimuon.costh) < 0.5 && 

// kDimuon cuts
abs(dimuon.dx) < 0.4 && abs(dimuon.dy-$YOFFS) < 0.3 && 
40 < dimuon.dpz && dimuon.dpz < 112 && 
abs(dimuon.dpx) < 2.0 && abs(dimuon.dpy) < 2.4 && 
dimuon.chisq_dimuon < 14 && 

// Target cuts
-250 < dimuon.dz && dimuon.dz < -20 && 
max(posTrack.chisq_target / posTrack.chisq_dump, negTrack.chisq_target / negTrack.chisq_dump) < 0.9 && 
max(posTrack.chisq_target/posTrack.chisq_upstream, negTrack.chisq_target/negTrack.chisq_upstream) < 0.8 && 
(posTrack.chisq_target/posTrack.chisq_dump) * (negTrack.chisq_target/negTrack.chisq_dump) < 0.7 && 
max(posTrack.chisq_target, negTrack.chisq_target) < 13 && 

// kTrack cuts
posTrack.roadID * negTrack.roadID < 0 && 
max(posTrack.chisq / (posTrack.nHits-5), negTrack.chisq / (negTrack.nHits-5)) < 12 && 
13 < min(posTrack.nHits, negTrack.nHits) && 
28 < posTrack.nHits + negTrack.nHits && 
10 < min(posTrack.pz1, negTrack.pz1) && max(posTrack.pz1, negTrack.pz1) < 75 && 
-300 < min(posTrack.z0, negTrack.z0) && max(posTrack.z0, negTrack.z0) < 20 && 
max(abs(posTrack.pxT),abs(negTrack.pxT)) < 4.5 && 
max(abs(posTrack.pyT),abs(negTrack.pyT)) < 3.0 && 
1 < min(sqrt(posTrack.pxT^2+posTrack.pyT^2),sqrt(negTrack.pxT^2+negTrack.pyT^2)) && 
max(abs(posTrack.xT*posTrack.pz1),abs(negTrack.xT*negTrack.pz1)) < 350 && 
max(abs((posTrack.yT-$YOFFS)*posTrack.pz1),abs((negTrack.yT-$YOFFS)*negTrack.pz1)) < 380 && 
max(abs(posTrack.yT-$YOFFS), abs(negTrack.yT-$YOFFS)) < 14 && 
max(abs(posTrack.yD-$YOFFS), abs(negTrack.yD-$YOFFS)) < 21 && 
max(negTrack.xT, -posTrack.xT) < 15 && 
max(negTrack.xD, -posTrack.xD) < 35 && 
max((negTrack.xD-negTrack.xT), -(posTrack.xD-posTrack.xT)) < 30 && 
16 < min((posTrack.xD)^2+(posTrack.yD-$YOFFS)^2, (negTrack.xD)^2+(negTrack.yD-$YOFFS)^2) && 
(posTrack.nHits > 15 || posTrack.pz1 > 17 || abs(posTrack.pyT/posTrack.pzT) > 0.018) && 
(negTrack.nHits > 14 || negTrack.pz1 > 16 || abs(negTrack.pyT/negTrack.pzT) > 0.017) && 

// Occupancy & new cuts
0 < min(posTrack.y1*posTrack.y3, negTrack.y1*negTrack.y3) && 
posTrack.y3 * negTrack.y3 < 0 && 
occupancy[0] < 400 && 
occupancy[1] < 400 && 
occupancy[2] < 450 && 

// Fix problem with bad tracking variables
sqrt((posTrack.xT)^2+(posTrack.yT-$YOFFS)^2) / sqrt((posTrack.xD)^2+(posTrack.yD-$YOFFS)^2) < 1 && 
sqrt((negTrack.xT)^2+(negTrack.yT-$YOFFS)^2) / sqrt((negTrack.xD)^2+(negTrack.yD-$YOFFS)^2) < 1 && 
