#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <string>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TCut.h>
#include <TString.h>
#include <TCanvas.h>
#include <TStyle.h>

#include "constants.h"
#include "DataStruct.h"
#include "readCut.C"

using namespace std;

int main(int argc, char* argv[])
{
    // collect args
    // TODO: improve interface, add 'Usage' info
    TString data_fn = argv[1];
    TString spill_fn = argv[2];

    // input cut file
    TCut cuts;
    if (argc > 3 && strlen(argv[3]) > 0) {
        cuts = readCut(argv[3]);
    }

    // optionally, specify additional cuts
    if (argc > 4 && strlen(argv[4]) > 0) {
        cuts += argv[4];
    }
    TCut spill_cut; // empty
    if (argc > 5 && strlen(argv[5]) > 0) {
        spill_cut = argv[5];
        cuts += spill_cut;
    }

    // define input data
    TFile* dataFile = new TFile(data_fn, "READ");
    TTree* dataTree = (TTree*)dataFile->Get("save");
    TFile* spillFile = new TFile(spill_fn, "READ");
    TTree* spillTree = (TTree*)spillFile->Get("save");

    // define the output file structure
    Dimuon* p_dimuon = new Dimuon; Dimuon& dimuon = *p_dimuon;
    Spill* p_spill = new Spill; Spill& spill = *p_spill;
    Event* p_event = new Event; Event& event = *p_event;
    Track* p_posTrack = new Track; Track& posTrack = *p_posTrack;
    Track* p_negTrack = new Track; Track& negTrack = *p_negTrack;

    cout << "Applying cut: " << cuts << endl;

    gROOT->cd(); // set current dir to gROOT before opening new trees (forces to memory)
    TTree* dataTreeCopy = dataTree->CopyTree(TString(cuts).ReplaceAll("$YOFFS", "1.6"));
    TTree* spillTreeCopy = spillTree->CopyTree(spill_cut);
    // close files, or sometimes get segfaults..
    dataFile->Close();
    spillFile->Close();

    cout << "Computing yields ..." << endl;
    spillTreeCopy->SetBranchAddress("spill", &p_spill);

    double lp[nTargets+1] = {0};
    double lp_err[nTargets+1] = {0};
    for(int i = 0; i < spillTreeCopy->GetEntries(); ++i) {
        spillTreeCopy->GetEntry(i);

        if(!spill.goodSpill()) continue;
        lp[spill.targetPos] += spill.liveG2SEM();
    }

    for(int i = 1; i <= nTargets; ++i) {
        lp_err[i] = 0; // assume zero statistical error for now. adjust later if needed
        cout << "Live protons, L" << i << " : " << lp[i] << " ± " << lp_err[i] << endl;
    }

    dataTreeCopy->SetBranchAddress("dimuon", &p_dimuon);
    dataTreeCopy->SetBranchAddress("event", &p_event);
    dataTreeCopy->SetBranchAddress("spill", &p_spill);
    dataTreeCopy->SetBranchAddress("posTrack", &p_posTrack);
    dataTreeCopy->SetBranchAddress("negTrack", &p_negTrack);

    double num_events[nTargets+1] = {0};
    for(int i = 1; i <= nTargets; ++i) {
        num_events[i] = dataTreeCopy->GetEntries(Form("targetPos==%d",i));
        cout << "No. entries, N" << i << " : " << num_events[i] << " ± " << sqrt(num_events[i]) << endl;
    }

    // reduced yield
    double yr[nTargets+1] = {0};
    double yr_err[nTargets+1] = {0};
    for(int i = 1; i <= nTargets; ++i) {
        yr[i] = num_events[i] / lp[i];
        yr_err[i] = yr[i] * sqrt( 1/num_events[i] + pow(lp_err[i]/lp[i],2) );
        cout << "Reduced yield, Y" << i << " : " << yr[i] << " ± " << yr_err[i] << endl;
    }

    const int tarTot = 1;
    const int tarBkg = 2;
    double diff = yr[tarTot] - yr[tarBkg];
    double diff_err = sqrt( pow(yr_err[tarTot],2) + pow(yr_err[tarBkg],2) );
    double snr = diff/yr[tarBkg];
    double snr_err = snr * sqrt( pow(diff_err/diff,2) + pow(yr_err[tarBkg]/yr[tarBkg],2) );

    cout << "Using targets : X=" << tarTot << ", E=" << tarBkg << endl;
    cout << "Diff: Y_X-Y_E = " << diff << " ± " << diff_err << " (" << diff_err/diff*100 << " %)" << endl;
    cout << "SNR: (Y_X-Y_E)/Y_X = " << snr << " ± " << snr_err << " (" << snr_err/snr*100 << " %)" << endl;

    return EXIT_SUCCESS;
}
