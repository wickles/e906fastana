#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <string>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TCut.h>
#include <TString.h>
#include <TCanvas.h>
#include <TStyle.h>

#include "constants.h"
#include "DataStruct.h"
#include "readCut.C"

using namespace std;

const char* out_dir = "results";

int main(int argc, char* argv[])
{
    bool do_mc = false;
    TFile* mcFile = NULL;
    TTree* mcTree = NULL;

    // collect args
    // TODO: improve interface, add 'Usage' info
    TString data_fn = argv[1];
    if (argc > 2 && strlen(argv[2]) > 0) {
        do_mc = true;
        mcFile = new TFile(argv[2], "READ");
        mcTree = (TTree*)mcFile->Get("save");
    }
    cout << "do mc? " << std::boolalpha << do_mc << endl;

    // input cut file
    TCut cuts;
    if (argc > 3 && strlen(argv[3]) > 0) {
        cuts = readCut(argv[3]);
    }
    // optionally, specify additional cuts
    if (argc > 4 && strlen(argv[4]) > 0) {
        cuts += argv[4];
    }

    // define input data
    TFile* dataFile = new TFile(data_fn, "READ");
    TTree* dataTree = (TTree*)dataFile->Get("save");

    cout << "Applying cut: " << cuts << endl;

    gROOT->cd(); // set current dir to gROOT before opening new trees (forces to memory)
    TTree* dataTreeCopy = dataTree;
    TTree* mcTreeCopy = mcTree;
    dataTreeCopy = dataTree->CopyTree(TString(cuts).ReplaceAll("$YOFFS", "1.6"));
    // close files, or sometimes get segfaults..
    dataFile->Close();
    if (do_mc) {
        mcTreeCopy = mcTree->CopyTree(TString(cuts).ReplaceAll("$YOFFS", "0"));
        mcFile->Close();
    }

    cout << "Making plots ..." << endl;
    ofstream fs (TString::Format("%s/cutcheck__selection.txt", out_dir).Data());
    fs << cuts << endl;
    fs.close();

    // define expressions to plot
    struct pquery {
        const char * varexp;
        const char * f_suffix;
        bool log_scale;
    };
    struct pquery queries[] = {
        {"mass", "dim_mass", 1},
        {"costh", "dim_costh", 0},
        {"dimuon.x2", "dim_x2", 0},
        {"dimuon.dz", "dim_dz", 0},
        {"dimuon.trackSeparation", "dim_trackSep", 0},
        {"dimuon.chisq_dimuon", "dim_chidim", 0},
        {"posTrack.xT", "xT__pos", 0},
        {"negTrack.xT", "xT__neg", 0},
        {"(posTrack.yT-$YOFFS)", "yT__pos", 0},
        {"(negTrack.yT-$YOFFS)", "yT__neg", 0},
        {"posTrack.xT * posTrack.pz1", "xT_times_pz1__pos", 0},
        {"negTrack.xT * negTrack.pz1", "xT_times_pz1__neg", 0},
        {"(posTrack.yT-$YOFFS) * posTrack.pz1", "yT_times_pz1__pos", 0},
        {"(negTrack.yT-$YOFFS) * negTrack.pz1", "yT_times_pz1__neg", 0},
        {"posTrack.xD", "xD__pos", 0},
        {"negTrack.xD", "xD__neg", 0},
        {"posTrack.yD-$YOFFS", "yD__pos", 0},
        {"negTrack.yD-$YOFFS", "yD__neg", 0},
        {"sqrt((posTrack.xD)^2 + (posTrack.yD-$YOFFS)^2)", "rD__pos", 0},
        {"sqrt((negTrack.xD)^2 + (negTrack.yD-$YOFFS)^2)", "rD__neg", 0},
        {"posTrack.chisq_target / posTrack.chisq_dump", "chit_over_chid__pos", 1},
        {"negTrack.chisq_target / negTrack.chisq_dump", "chit_over_chid__neg", 1},
        {"posTrack.chisq_target / posTrack.chisq_upstream", "chit_over_chiu__pos", 1},
        {"negTrack.chisq_target / negTrack.chisq_upstream", "chit_over_chiu__neg", 1},
        {"posTrack.z0x", "z0x__pos", 1},
        {"negTrack.z0x", "z0x__neg", 1},
        {"posTrack.pz1", "pz1__pos", 1},
        {"negTrack.pz1", "pz1__neg", 1},
        {"posTrack.chisq / (posTrack.nHits-5)", "chik_over_dof__pos", 1},
        {"negTrack.chisq / (negTrack.nHits-5)", "chik_over_dof__neg", 1},
        {"posTrack.chisq_target", "chit__pos", 1},
        {"negTrack.chisq_target", "chit__neg", 1},
        {"sqrt(posTrack.pxT^2 + posTrack.pyT^2)", "pT__pos", 1},
        {"sqrt(negTrack.pxT^2 + negTrack.pyT^2)", "pT__neg", 1},
        {"posTrack.pxT", "pxT__pos", 1},
        {"negTrack.pxT", "pxT__neg", 1},
        {"posTrack.pyT", "pyT__pos", 1},
        {"negTrack.pyT", "pyT__neg", 1},
        {"negTrack.nHits", "nHits__neg", 0},
        {"posTrack.nHits", "nHits__pos", 0},
    };
    const int num_query = sizeof(queries) / sizeof(*queries);

    // plot desired expressions for data (and MC, optionally)
    TCanvas* c1 = new TCanvas();
    const int numbins = 60;

    for (int i = 0; i < num_query; ++i) {
        gPad->SetLogy(queries[i].log_scale);
        gStyle->SetStatY(0.7);
        long long numDat = dataTreeCopy->Draw(Form("%s>>hdat(%d,0,0)", TString(queries[i].varexp).ReplaceAll("$YOFFS", "1.6").Data(), numbins), "");
        TH1F* hdat = (TH1F*)gPad->GetPrimitive("hdat");
        hdat->SetLineStyle(1);
        hdat->GetXaxis()->SetTitle(queries[i].varexp);
        hdat->SetTitle("");
        gPad->Update();
        if (do_mc) {
            gStyle->SetStatY(0.9);
            long long numMC = mcTreeCopy->Draw(Form("%s>>hmc(%d,0,0)", TString(queries[i].varexp).ReplaceAll("$YOFFS", "0").Data(), numbins), "weight", "SAMES HIST");
            TH1F* hmc = (TH1F*)gPad->GetPrimitive("hmc");
            hmc->SetLineStyle(2);
            hmc->Scale((double)numDat / hmc->Integral());
            gPad->Update();
        }
        TString filename = TString::Format("%s/cutcheck_%s.pdf", out_dir, queries[i].f_suffix);
        c1->Print(filename);
    }

    return EXIT_SUCCESS;
}
