kTrack cuts
|xT*pz1|<300, |(yT-1.6)*pz1|<300, |xD|<30, |yD-1.6|<20
3.5<sqrt(xD*xD+(yD-1.6)*(yD-1.6))<30
chisq_target/chisq_dump<.6, chisq_target/chisq_upstream<.8
-280<z0x><20, 13<pz1<76, chisq/(numHits-5)<15, chisq_target<14
 |pxT|<5, |pyT|<4, sqrt(pxT*pxT+pyT*pyT)>1.2
Note, use yT and yD, not yT-1.6 or yD-1.6 for MC events

kDimuon cuts
xF>-0.2, .2<xT<.6, 4<mass<9, |costh|<.5, chisq_dimuon<15
 |dx|<1, |dy|<1, |dpx|<4, |dpy|<3, dpz<110, opposite charge, Top*Bottom
