#include <iostream>
#include <cmath>

#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <TCut.h>

#include "constants.h"
#include "DataStruct.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 2) {
        printf("Usage: %s spill_file [spill_cut]\n", argv[0]);
        return EXIT_FAILURE;
    }
    const char * spill_file = argv[1];
    const char * spill_cut = argv[2];

    TFile* spillFile = new TFile(spill_file, "READ");
    TTree* spillTree = (TTree*)spillFile->Get("save");
    TTree* spillTreeCopy = spillTree;

    if (argc > 2 && strlen(spill_cut) > 0) {
        TCut spill_cut = spill_cut;
        gROOT->cd(); // forces tree to memory (required to do CopyTree)
        spillTreeCopy = spillTree->CopyTree(spill_cut);
    }

    // define the output file structure
    Spill* p_spill = new Spill; Spill& spill = *p_spill;
    spillTreeCopy->SetBranchAddress("spill", &p_spill);

    double np0[nTargets+1] = {0};
    double np1[nTargets+1] = {0};
    for(int i = 0; i < spillTreeCopy->GetEntries(); ++i)
    {
        spillTreeCopy->GetEntry(i);

        if(!spill.goodSpill()) continue;
        // "standard" value from SQL
        np0[spill.targetPos] += spill.liveProton;
        // "corrected" value with adjusted pedestal
        np1[spill.targetPos] += spill.liveG2SEM();
    }

    cout << "Live protons (standard/SQL):   ";
    for (int i = 1; i <= nTargets; i++) {
        cout << np0[i];
        if (i < nTargets) {
            cout << ", ";
        }
    }
    cout << endl;

    cout << "Live protons (corrected):      ";
    for (int i = 1; i <= nTargets; i++) {
        cout << np1[i];
        if (i < nTargets) {
            cout << ", ";
        }
    }
    cout << endl;

    return EXIT_SUCCESS;
}
