// physics cuts
0.18 < dimuon.x2 && dimuon.x2 < 0.64 && 
4 < dimuon.mass && dimuon.mass < 8.7 && 
dimuon.xF > -0.2 && 
abs(dimuon.costh) < 0.52 && 

// kDimuon cuts
dimuon.dpz < 110 && 
abs(dimuon.dpx) < 4 && abs(dimuon.dpy) < 3 && 
abs(dimuon.dx) < 0.7 && abs(dimuon.dy) < 0.7 && 
dimuon.chisq_dimuon < 16 && 

// Target cuts
dimuon.dz < 0 && 
max(posTrack.chisq_target / posTrack.chisq_dump, negTrack.chisq_target / negTrack.chisq_dump) < 0.6 && 
max(posTrack.chisq_target/posTrack.chisq_upstream, negTrack.chisq_target/negTrack.chisq_upstream) < 0.8 && 
max(posTrack.chisq_target, negTrack.chisq_target) < 13 && 

// kTrack cuts
posTrack.roadID * negTrack.roadID < 0 && 
max(posTrack.chisq / (posTrack.nHits-5), negTrack.chisq / (negTrack.nHits-5)) < 13 && 

12 < min(posTrack.pz1, negTrack.pz1) && max(posTrack.pz1, negTrack.pz1) < 78 && 
-320 < min(posTrack.z0x, negTrack.z0x) && max(posTrack.z0x, negTrack.z0x) < 10 && 
max(abs(posTrack.pxT),abs(negTrack.pxT)) < 5 && 
max(abs(posTrack.pyT),abs(negTrack.pyT)) < 4 && 
1.2 < min(sqrt(posTrack.pxT^2+posTrack.pyT^2),sqrt(negTrack.pxT^2+negTrack.pyT^2)) && 
max(sqrt(posTrack.pxT^2+posTrack.pyT^2),sqrt(negTrack.pxT^2+negTrack.pyT^2)) < 10 && 
max(abs(posTrack.xT*posTrack.pz1),abs(negTrack.xT*negTrack.pz1)) < 300 && 
max(abs((posTrack.yT-$YOFFS)*posTrack.pz1),abs((negTrack.yT-$YOFFS)*negTrack.pz1)) < 320 && 
max(abs(posTrack.xD), abs(negTrack.xD)) < 32 && 
max(abs(posTrack.yD-$YOFFS), abs(negTrack.yD-$YOFFS)) < 22 && 
3.5 < min(sqrt((posTrack.xD)^2+(posTrack.yD-$YOFFS)^2), sqrt((negTrack.xD)^2+(negTrack.yD-$YOFFS)^2)) 
