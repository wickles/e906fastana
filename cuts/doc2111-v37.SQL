-- Updated set of cuts after study of R008 high/low Occupancy and Monte Carlo runs versus db3/db01 R008 runs, 8/14/2018  

-- These cuts are specifically designed to help reduce randoms and dump feedthroughs – accomplished by comparing events with Occupancy.D1+D2+D3 <359.5 or Occupancy.D1+D2+D3 >359.5 and comparing targetPos 1,3 with 2,4
-- Note: the beam offset in yT, and yD  (and maybe xT and xD ? ) must be adjusted for each data set.

FROM kDimuon a, kTrack b, kTrack c, Occupancy(R007) d
WHERE a.spillID=b.spillID and a.eventID=b.eventID and a.spillID=c.spillID and a.eventID=c.eventID and a.targetPos>0  
and a.runID=d.runID and a.spillID=d.spillID and a.eventID=d.eventID and d.D1<400 and d.D2<400 and d.D3<400 

and abs(a.dx)<.25 and abs(a.dy-1.6)<.22 and a.dz>-280 and a.dz<-5 and abs(a.dpx)<1.8 and abs(a.dpy)<2  
and a.dpx*a.dpx+a.dpy*a.dpy<5 and a.dpz>38 and a.dpz<116 and a.mass>4.2 and a.mass<8.8  
and a.dx*a.dx+(a.dy-1.6)*(a.dy-1.6)<.08 and a.xF<.95 and a.xF>-.1 and a.xT>.05 and a.xT<.5 and abs(a.costh)<.5 
and abs(a.trackSeparation)<270 and a.chisq_dimuon<17 

and b.chisq_target<14 and b.pz1>9 and b.pz1<75 and b.numHits>13 and b.xT*b.xT+(b.yT-1.6)*(b.yT-1.6)<320 
and b.xD*b.xD+(b.yD-1.6)*(b.yD-1.6)<1100 and b.xD*b.xD+(b.yD-1.6)*(b.yD-1.6)>16 and abs(b.roadID)<55000 
and b.chisq_target<1.5*b.chisq_upstream and b.chisq_target<1.5*b.chisq_dump and b.charge=+1 and b.z0<-5 and b.z0>-320 
and b.chisq/(b.numHits-5)<12 and atan(b.y3/b.y1)>.7 

and c.chisq_target<14 and c.pz1>9 and c.pz1<75 and c.numHits>13 and c.xT*c.xT+(c.yT-1.6)*(c.yT-1.6)<320
and c.xD*c.xD+(c.yD-1.6)*(c.yD-1.6)<1100 and c.xD*c.xD+(c.yD-1.6)*(c.yD-1.6)>16 
and c.chisq_target<1.5*c.chisq_upstream and c.chisq_target<1.5*c.chisq_dump and c.charge=-1 and c.z0<-5 and c.z0>-320 
and c.chisq/(c.numHits-5)<12 and atan(c.y3/c.y1)<.7 

and abs(b.px1-b.px3+.416)<.008 and abs(c.px1-c.px3-.416)<.008 and abs(b.py1-b.py3)<.008 and abs(c.py1-c.py3)<.008 
and abs(b.pz1-b.pz3)<.08 and abs(c.pz1-c.pz3)<.08 and abs(b.chisq_target+c.chisq_target-chisq_dimuon)<2 
and b.y1*b.y3>0 and c.y1*c.y3>0 and b.y3*c.y3<0 and b.numHits+c.numHits>29 and b.numHitsSt1+c.numHitsSt1>8 

-- cuts to specifically reduce randoms --
and least(b.pz1,c.pz1)-greatest(b.x1,-c.x1)/4.5>12 and least(abs(b.py1),abs(c.py1))>.05

