-- TODO: clean up, change aliases
AND a.dz>-250 AND a.dz<-20 AND a.dpz<112 AND a.dpz>40 
AND a.chisq_dimuon<14 AND abs(a.costh)<.5 
AND a.xF<.9 AND a.xF>-.2 
AND d.D1<400 
AND a.mass>4. AND a.mass<8.5 
AND abs(a.dy-1.6)<.24 AND abs(a.dx)<.26 
AND abs(a.dpx)<2.2 AND abs(a.dpy)<2.4 
AND b.numHits+c.numHits>29 AND a.xT>.07 AND b.numHits>13 
AND b.chisq/(b.numHits-5)<12 
AND abs(b.yT-1.6)<14 AND abs(b.yD-1.6)<21 
AND b.chisq_target<.9*b.chisq_dump 
AND c.numHits>13 AND c.chisq/(c.numHits-5)<12 
AND abs(c.yT-1.6)<14 AND abs(c.yD-1.6)<21 AND d.D2<400 
AND c.chisq_target<.9*c.chisq_dump 
AND b.z0<10 AND b.z0>-290 
AND b.xD*b.xD+(b.yD-1.6)*(b.yD-1.6)>16 
AND abs(b.pxt)<4.5 AND abs(b.pyt)<3 
AND b.pz1<75 AND b.pz1>10 
AND c.z0<10 AND c.z0>-290 
AND c.xD*c.xD+(c.yD-1.6)*(c.yD-1.6)>16 
AND abs(c.pxt)<4.5 AND abs(c.pyt)<3 
AND c.pz1<75 AND c.pz1>10 
AND abs(b.xT*b.pz1)<350 AND abs((b.yT-1.6)*b.pz1)<350 
AND b.xT>-15 AND b.xD>-35 AND b.xD-b.xT>-30 
AND b.chisq_target<.8*b.chisq_upstream 
AND abs(c.xT*c.pz1)<350 AND abs((c.yT-1.6)*c.pz1)<350 
AND c.xT<15. AND c.xD<35. AND c.xD-c.xT<30. 
AND c.chisq_target<.8*c.chisq_upstream 
AND b.pxt*b.pxt+b.pyt*b.pyt>1 
AND b.y1*b.y3>0 
AND b.chisq_target<13 
AND d.D3<450
AND (b.numHits>14 OR b.pz1>17 OR abs(b.pyT/b.pzT)>.016) 
AND c.pxt*c.pxt+c.pyt*c.pyt>1 
AND c.y1*c.y3>0 
AND c.chisq_target<13 
AND (c.numHits>14 OR c.pz1>17 OR abs(c.pyT/c.pzT)>.015) 
AND b.y3*c.y3<0 
AND (b.chisq_target/b.chisq_dump)*(c.chisq_target/c.chisq_dump)<.4 
AND a.targetPos>0 AND a.targetPos<5 
AND abs(b.yT-c.yT)<22 AND abs(b.xT-c.xT)<22
