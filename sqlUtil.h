#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <map>

#include <TString.h>

#include "DataStruct.h"

// type GOBJECT must be defined in source before include
GOBJECT* g906;

int getInt(const char* row, const double errval = ERR_VAL_G) {
    if(row == NULL) {
        g906->log("Integer content is missing.");
        return errval;
    }
    return atoi(row);
}

// TODO: use NAN as default?
double getFloat(const char* row, const double errval = ERR_VAL_G) {
    if(row == NULL) {
        g906->log("Floating content is missing.");
        return errval;
    }
    return atof(row);
}


const char * seaquel_user = "seaguest";
const char * seaquel_passwd = "qqbar2mu+mu-";

#define HOST_E906_2 "e906-db2.fnal.gov"
#define HOST_E906_3 "e906-db3.fnal.gov"
#define HOST_SEAQUEST_1 "seaquestdb01.fnal.gov"
#define HOST_UIUC "seaquel.physics.illinois.edu"
#define PORT_SEAQUEST_1 "3310"
#define PORT_UIUC "3283"
#define SRV_E906_3 HOST_E906_3
#define SRV_SEAQUEST_1 HOST_SEAQUEST_1 ":" PORT_SEAQUEST_1
#define SRV_UIUC HOST_UIUC ":" PORT_UIUC

std::string expand_server(std::string server) {
    if (server == HOST_E906_2) {
        server = HOST_SEAQUEST_1;
    }
    if (server == HOST_SEAQUEST_1) {
        server = SRV_SEAQUEST_1;
    }
    if (server == HOST_UIUC) {
        server = SRV_UIUC;
    }
    return server;
}

struct run_info {
    run_info() {}
    TString server;
    TString schema;
};

int get_runlist(std::map<int, run_info>& ret, const char * fname) {
    if (fname == NULL) return -1;

    std::ifstream listf(fname);
    std::string line;
    std::string server, schema;
    int run_id;

    if (listf.is_open()) {
        while (getline(listf, line)) {
            if (line[0] != '#') {
                std::stringstream ss(line);
                ss >> server >> schema; // >> roadset >> schema_ktrack;
                std::sscanf(schema.c_str(), "run_%d_*", &run_id);
                //cout << run_id << " / " << server << " / " << schema << endl;
                //server = expand_server(server);
                run_info ri;
                ri.server = server.c_str();
                ri.schema = schema.c_str();
                ret[run_id] = ri;
            }
        }
        listf.close();
    }

    return 0;
}

class sql_connector
{
private:
    std::map<std::string, TSQLServer*> conmap;
public:
    sql_connector() {}
    ~sql_connector() {
        for (std::map<std::string, TSQLServer*>::iterator it=conmap.begin(); it!=conmap.end(); ++it) {
            if (it->second != NULL) {
                delete it->second;
            }
        }
    }
    TSQLServer* getcon(const char * host) {
        TSQLServer*& con = conmap[host];
        // if connection exists, check status and delete if not connected
        if (con != NULL && !con->PingVerify()) {
            std::cout << "Deleting connection: " << host << std::endl;
            delete con;
            con = NULL;
        }
        if (con == NULL) {
            std::cout << "Connecting to: " << host << std::endl;
            con = TSQLServer::Connect(host, seaquel_user, seaquel_passwd);
        }
        return con;
    }
    TSQLResult* query(const char * host, const char * sql) {
        TSQLServer* con = getcon(host);
        return conmap[host]->Query(sql);
    }
};

