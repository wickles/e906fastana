#pragma once

#include <TMath.h>

// CODATA 2014
const double Avo = 6.022140857e23; // uncertainty: (74)

// Targets 1-7: LH2, empty flask, LD2, no target, Fe, C, W
// Target 8 sometimes used to refer to H target used in place of D target
const int nTargets = 7;
const char* targetName[] = {"", "H", "Flask", "D", "None", "Fe", "C", "W"};
const char* targetNameLong[] = {"", "LH2", "Flask", "LD2", "None", "Iron", "Carbon", "Tungsten"};

// nuclear properties. source: PDG 2017, E906 measurements

// Atomic mass number
const double A_H = 1.00794;     // (7)
const double A_D = 2.014101764; // (13)
const double A_Fe = 55.845;     // (2)
const double A_C = 12.0107;     // (8)
const double A_W = 183.84;      // (1)
const double A[] = {0., A_H, 0., A_D, 0., A_Fe, A_C, A_W};

// Atomic proton number
const double Z_H = 1.;
const double Z_D = 1.;
const double Z_Fe = 26.;
const double Z_C = 6.;
const double Z_W = 74.;
const double Z[] = {0., Z_H, 0., Z_D, 0., Z_Fe, Z_C, Z_W};

// Density (g/cm^3)
const double rho_H = 0.07065;
const double rho_D = 0.1617;
const double rho_Fe = 7.874;
const double rho_C = 1.802;
const double rho_W = 19.3;
const double rho[] = {0., rho_H, 0., rho_D, 0., rho_Fe, rho_C, rho_W};

// Nuclear interaction length (g/cm^2)
const double ell_H = 52.0;
const double ell_D = 71.8;
const double ell_Fe = 132.1;
const double ell_C = 85.8;
const double ell_W = 191.9;
const double ell[] = {0., ell_H, 0., ell_D, 0., ell_Fe, ell_C, ell_W};

// Nuclear interaction length (cm)
const double lambda_H = ell_H/rho_H;
const double lambda_D = ell_D/rho_D;
const double lambda_Fe = ell_Fe/rho_Fe;
const double lambda_C = ell_C/rho_C;
const double lambda_W = ell_W/rho_W;
const double lambda[] = {0., lambda_H, 0., lambda_D, 0., lambda_Fe, lambda_C, lambda_W};

// Effective target length (cm)
const double L_H = 20.*2.54;
const double L_D = 20.*2.54;
const double L_Fe = 0.250*3.*2.54;
const double L_C = 0.436*3.*2.54;
const double L_W = 0.125*3.*2.54;
const double L[] = {0., L_H, 0., L_D, 0., L_Fe, L_C, L_W};

// integrated target intensity factor
const double ratio_H = 1. - TMath::Exp(-L_H/lambda_H);
const double ratio_D = 1. - TMath::Exp(-L_D/lambda_D);
const double ratio_Fe = 1. - TMath::Exp(-L_Fe/lambda_Fe);
const double ratio_C = 1. - TMath::Exp(-L_C/lambda_C);
const double ratio_W = 1. - TMath::Exp(-L_W/lambda_W);
const double ratioBeam[] = {0., ratio_H, 0., ratio_D, 0., ratio_Fe, ratio_C, ratio_W};

// average target intensity attenuation
//double atten = lambda[tar] / L[tar] * ratioBeam[tar];

