// kDimuon cuts
30 < dimuon.dpz && dimuon.dpz < 120 && 
abs(dimuon.dpx) < 3 && abs(dimuon.dpy) < 3 && 
abs(dimuon.dx) < 2 && abs(dimuon.dy) < 2 && 
dimuon.chisq_dimuon < 15 && 
abs(dimuon.trackSeparation) < 250 && 

// Target cut
-300 < dimuon.dz && dimuon.dz < -60 && 
max(posTrack.chisq_target - posTrack.chisq_dump, negTrack.chisq_target - negTrack.chisq_dump) < -10 && 

// kTrack cuts
posTrack.roadID * negTrack.roadID < 0 && 
14 < min(posTrack.nHits, negTrack.nHits) && 
max(posTrack.chisq / (posTrack.nHits-5), negTrack.chisq / (negTrack.nHits-5)) < 5 && 
(18 < posTrack.pz1 || 18 <= posTrack.nHits) && 
(18 < negTrack.pz1 || 18 <= negTrack.nHits) && 
-400 < min(posTrack.z0, negTrack.z0) && max(posTrack.z0, negTrack.z0) < 200 && 
