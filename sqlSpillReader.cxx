#include <iostream>
#include <cmath>
#include <stdlib.h>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TSQLServer.h>
#include <TSQLResult.h>
#include <TSQLRow.h>

#define GOBJECT Spill
#include "sqlUtil.h"
#include "DataStruct.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 4) {
        // not enough arguments
        printf("Usage: %s server schema output_file\n", argv[0]);
        return -1;
    }
    // collect args
    TString server_name = argv[1];
    TString schema_main = argv[2];
    TString fout_dat = argv[3];
    TString schema_ktrack = schema_main;
    if (argc > 4) {
        schema_ktrack = argv[4];
    }

    // define the output file structure
    Spill* p_spill = new Spill; Spill& spill = *p_spill;
    spill.skipflag = false;

    g906 = p_spill;

    TFile* saveFile = new TFile(fout_dat, "new");
    TTree* saveTree = new TTree("save", "save");
    if (saveFile->IsZombie()) return -1;

    saveTree->Branch("spill", &p_spill, 256000, 99);

    //Connect to server
    TSQLServer* server = TSQLServer::Connect(Form("mysql://%s", server_name.Data()), seaquel_user, seaquel_passwd);
    server->Exec(Form("USE %s", schema_main.Data()));
    cout << "Reading schema " << schema_main << " and save to " << fout_dat << endl;

    TString query = "SELECT runID,spillID FROM Spill ORDER BY spillID";

    TSQLResult* res = server->Query(query);
    int nSpillsRow = res->GetRowCount();

    int nBadSpill_record = 0;
    int nBadSpill_duplicate = 0;
    int nBadSpill_quality = 0;
    for(int i = 0; i < nSpillsRow; ++i)
    {
        if(i % 100 == 0)
        {
            cout << "Converting spill " << i << "/" << nSpillsRow << endl;
        }

        //basic spillID info
        TSQLRow* row = res->Next();
        spill.runID = getInt(row->GetField(0));
        spill.spillID = getInt(row->GetField(1));
        spill.trigSet = spill.triggerSet();
        delete row;

        //magnet configuration
        //FIXME: these can be wrong? According to Kun. Check with Chuck
        TString query = Form("SELECT value FROM Beam WHERE spillID=%d AND name IN ('F:NM3S','F:NM4AN') ORDER BY name", spill.spillID);
        TSQLResult* res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 2)
        {
            spill.log(Form("lacks magnet info %d", res_spill->GetRowCount()));
            res_spill->GetRowCount() > 2 ? ++nBadSpill_duplicate : ++nBadSpill_record;

            delete res_spill;
            continue;
        }

        TSQLRow* row_spill = res_spill->Next(); spill.FMAG = getFloat(row_spill->GetField(0)); delete row_spill;
        row_spill = res_spill->Next();          spill.KMAG = getFloat(row_spill->GetField(0)); delete row_spill;
        delete res_spill;

        //EventID range
        query = Form("SELECT MIN(eventID),MAX(eventID) FROM Event WHERE runID=%d AND spillID=%d", spill.runID, spill.spillID);
        res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 1)
        {
            spill.log(Form("lacks event table entry %d", res_spill->GetRowCount()));
            res_spill->GetRowCount() > 1 ? ++nBadSpill_duplicate : ++nBadSpill_record;

            delete res_spill;
            continue;
        }

        row_spill = res_spill->Next();
        spill.eventID_min = getInt(row_spill->GetField(0));
        spill.eventID_max = getInt(row_spill->GetField(1));
        delete row_spill;
        delete res_spill;

        //target position
        query = Form("SELECT a.targetPos,b.value,a.dataQuality,a.liveProton FROM Spill AS a,Target AS b WHERE a.spillID"
            "=%d AND b.spillID=%d AND b.name='TARGPOS_CONTROL'", spill.spillID, spill.spillID);
        res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 1)
        {
            spill.log(Form("lacks target position info %d", res_spill->GetRowCount()));
            res_spill->GetRowCount() > 1 ? ++nBadSpill_duplicate : ++nBadSpill_record;

            delete res_spill;
            continue;
        }

        row_spill = res_spill->Next();
        spill.targetPos       = getInt(row_spill->GetField(0));
        spill.TARGPOS_CONTROL = getInt(row_spill->GetField(1));
        spill.quality         = getInt(row_spill->GetField(2));
        spill.liveProton      = getFloat(row_spill->GetField(3));
        delete row_spill;
        delete res_spill;

        //Reconstruction info
        query = Form("SELECT (SELECT COUNT(*) FROM Event WHERE spillID=%d),"
            "(SELECT COUNT(*) FROM %s.kDimuon WHERE spillID=%d),"
            "(SELECT COUNT(*) FROM %s.kTrack WHERE spillID=%d)",
            spill.spillID, schema_ktrack.Data(), spill.spillID, schema_ktrack.Data(), spill.spillID);
        res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 1)
        {
            spill.log(Form("lacks reconstructed tables %d", res_spill->GetRowCount()));
            res_spill->GetRowCount() > 1 ? ++nBadSpill_duplicate : ++nBadSpill_record;

            delete res_spill;
            continue;
        }

        row_spill = res_spill->Next();
        spill.nEvents = getInt(row_spill->GetField(0));
        spill.nDimuons = getInt(row_spill->GetField(1));
        spill.nTracks = getInt(row_spill->GetField(2));
        delete row_spill;
        delete res_spill;

        //Beam/BeamDAQ
        query = Form("SELECT a.value,b.NM3ION,b.QIESum,b.inhibit_block_sum,b.trigger_sum_no_inhibit,b.dutyFactor53MHz,b.inh_thres "
            "FROM Beam AS a,BeamDAQ AS b WHERE a.spillID=%d AND b.spillID=%d AND a.name='S:G2SEM'", spill.spillID, spill.spillID);
        res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 1)
        {
            spill.log(Form("lacks Beam/BeamDAQ info %d", res_spill->GetRowCount()));
            res_spill->GetRowCount() > 1 ? ++nBadSpill_duplicate : ++nBadSpill_record;

            delete res_spill;
            continue;
        }

        row_spill = res_spill->Next();
        spill.G2SEM      = getFloat(row_spill->GetField(0));
        spill.NM3ION     = getFloat(row_spill->GetField(1));
        spill.QIESum     = getFloat(row_spill->GetField(2));
        spill.inhibitSum = getFloat(row_spill->GetField(3));
        spill.busySum    = getFloat(row_spill->GetField(4));
        spill.dutyFactor = getFloat(row_spill->GetField(5));
        spill.inh_thres  = getInt(row_spill->GetField(6));

        delete row_spill;
        delete res_spill;

        //Scalar table -- EOS
        query = Form("SELECT value FROM Scaler WHERE spillType='EOS' AND spillID=%d AND scalerName "
            "in ('TSGo','AcceptedMatrix1','AfterInhMatrix1') ORDER BY scalerName", spill.spillID);
        res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 3)
        {
            spill.log(Form("lacks scaler info %d", res_spill->GetRowCount()));
            res_spill->GetRowCount() > 3 ? ++nBadSpill_duplicate : ++nBadSpill_record;

            delete res_spill;
            continue;
        }

        row_spill = res_spill->Next(); spill.acceptedMatrix1 = getFloat(row_spill->GetField(0)); delete row_spill;
        row_spill = res_spill->Next(); spill.afterInhMatrix1 = getFloat(row_spill->GetField(0)); delete row_spill;
        row_spill = res_spill->Next(); spill.TSGo            = getFloat(row_spill->GetField(0)); delete row_spill;
        delete res_spill;

        //Scalar table -- BOS
        query = Form("SELECT value FROM Scaler WHERE spillType='BOS' AND spillID=%d AND scalerName "
            "in ('TSGo','AcceptedMatrix1','AfterInhMatrix1') ORDER BY scalerName", spill.spillID);
        res_spill = server->Query(query);
        if(res_spill->GetRowCount() != 3)
        {
            spill.log(Form("lacks scaler info %d", res_spill->GetRowCount()));
            spill.acceptedMatrix1BOS = -1;
            spill.afterInhMatrix1BOS = -1;
            spill.TSGoBOS = -1;
        }
        else
        {
            row_spill = res_spill->Next(); spill.acceptedMatrix1BOS = getFloat(row_spill->GetField(0)); delete row_spill;
            row_spill = res_spill->Next(); spill.afterInhMatrix1BOS = getFloat(row_spill->GetField(0)); delete row_spill;
            row_spill = res_spill->Next(); spill.TSGoBOS            = getFloat(row_spill->GetField(0)); delete row_spill;
        }
        delete res_spill;

        if(!spill.goodSpill())
        {
            ++nBadSpill_quality;
            spill.log("spill bad");
            spill.print();
            // TODO: Don't write bad spills? (quality bits)
            // i.e. continue; here
        }

        //Save
        saveTree->Fill();
    }
    delete res;

    cout << "sqlSpillReader finished successfully." << endl;
    cout << saveTree->GetEntries() << " good spills, " << nBadSpill_record << " spills have insufficient info in database, "
         << nBadSpill_duplicate << " spills have duplicate entries in database, " << nBadSpill_quality << " rejected because of bad quality." << endl;

    saveFile->cd();
    saveTree->Write();
    saveFile->Close();

    return EXIT_SUCCESS;
}
