#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TSQLServer.h>
#include <TSQLResult.h>
#include <TSQLRow.h>

#define GOBJECT Event
#include "sqlUtil.h"
#include "DataStruct.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 4) {
        // not enough arguments
        cout << Form("Usage: %s server schema output_file [spill_file] [sql_cut_track] [sql_extra] [suffix] [ktrack_schema] [embed_source_schema|runlist] [embed_source_server] [embed_info_schema]", argv[0]) << endl;
        cout << " - For MC mode, pass 'mc' to [suffix]." << endl;
        cout << " - For extra track cuts, pass 'AND [condition]' to [sql_cut_track]." << endl;
        cout << " - To limit number of selected tracks, pass 'LIMIT [num]' to [sql_extra]." << endl;
        cout << " - For messy MC, pass runlist if unmerged, or source schema and server (if different from main server)." << endl;
        cout << " - For clean MC, pass options for messy MC and schema with kEmbed (if different from kTrack schema)." << endl;
        cout << " - For embedded source data on single server, can pass C-style formatted string for [embed_source_schema], e.g. 'run_%06d_R008'." << endl;
        return -1;
    }
    // collect args
    TString server_name = argv[1];
    TString schema_main = argv[2];
    TString fout_dat = argv[3];
    TString schema_ktrack = schema_main;
    // --- optional arguments follow ---
    TString spill_file, suffix, sql_cut_track, sql_extra, server_alt, schema_embed_source, schema_embed_info;
    int is_embed = 0;
    int embed_runner = 0; // type of embed: 0 (merged), 1 (unmerged, single server), 2 (unmerged, runlist for server/schema)
    map<int, run_info> runlist;
    if (argc > 4) {
        // spill file
        spill_file = argv[4];
    }
    if (argc > 5) {
        // WHERE queries, e.g. "AND runID > 100 AND runID < 200"
        sql_cut_track = argv[5];
    }
    if (argc > 6) {
        // extra terminating commands, e.g. "LIMIT 10000"
        sql_extra = argv[6];
    }
    if (argc > 7) {
        // table suffix, e.g. "Mix" (modifies behavior)
        suffix = argv[7];
    }
    if (argc > 8) {
        // schema with kTracker tables
        // schema_main should contain event-level and MC tables
        schema_ktrack = argv[8];
    }
    if (argc > 9) {
        schema_embed_source = argv[9];
        schema_embed_info = schema_ktrack;
        server_alt = server_name;
        is_embed = 1;
        // if data is unmerged, pass embed schema with a C-style formatting token for the runID
        // e.g. 'run_%06d_R00X'
        if (schema_embed_source.Contains("%")) {
            embed_runner = 1;
        }
        // or pass a runlist filename
        if (schema_embed_source.EndsWith(".list") || schema_embed_source.EndsWith(".txt")) {
            embed_runner = 2;
            get_runlist(runlist, schema_embed_source);
        }
    }
    if (argc > 10) {
        server_alt = argv[10];
    }
    if (argc > 11) {
        schema_embed_info = argv[11];
        // set to 2 to indicate clean MC
        is_embed = 2;
    }

    TFile* saveFile = new TFile(fout_dat, "new");
    TTree* saveTree = new TTree("save", "save");
    if (saveFile->IsZombie()) return -1;

    // define the output file structure
    Spill* p_spill = new Spill; Spill& spill = *p_spill;
    Event* p_event = new Event; Event& event = *p_event;
    Track* p_track = new Track; Track& track = *p_track;

    g906 = p_event;

    saveTree->Branch("event", &p_event, 256000, 99);
    saveTree->Branch("spill", &p_spill, 256000, 99);
    saveTree->Branch("track", &p_track, 256000, 99);

    //Connect to server
    TSQLServer* server = TSQLServer::Connect(Form("mysql://%s", server_name.Data()), seaquel_user, seaquel_passwd);
    server->Exec(Form("USE %s", schema_main.Data()));
    cout << "Reading from schema: " << schema_main << endl;
    cout << "Writing to file: " << fout_dat << endl;

    //Connect to alt server if necessary
    TSQLServer* srv_alt = NULL;
    if (is_embed && embed_runner == 1) {
        srv_alt = TSQLServer::Connect(Form("mysql://%s", server_alt.Data()), seaquel_user, seaquel_passwd);
    }

    // store connections for embed servers .. TODO: generalize to all connections used
    sql_connector connector;

    //decide how to access event/spill level information
    spill.skipflag = suffix.Contains("Mix", TString::kIgnoreCase);
    // if suffix arg contains "mc", set spill skipflag and mcflag
    bool mcflag = false;
    if (suffix.Contains("mc", TString::kIgnoreCase)) {
        mcflag = true;
        suffix = "";
        spill.skipflag = true;
        // query and print some info
        TSQLResult* res_info = server->Query(Form("SELECT * FROM %s.kInfo "
                    "WHERE infoKey IN ('version', 'kTrackerVer', 'Geometry', 'Realization')", schema_ktrack.Data()));
        TSQLRow* row_info = NULL;
        cout << " -- MC info -- " << endl;
        for (int i = 0; i < res_info->GetRowCount(); ++i) {
            row_info = res_info->Next();
            cout << row_info->GetField(0) << " : " << row_info->GetField(1) << endl;
        }
        delete row_info;
        delete res_info;
    }

    //pre-load spill information if provided
    map<int, Spill> spillBank;
    if(!spill.skipflag)
    {
        TFile* spillFile = new TFile(spill_file);
        TTree* spillTree = (TTree*)spillFile->Get("save");
        spillTree->SetBranchAddress("spill", &p_spill);

        for(int i = 0; i < spillTree->GetEntries(); ++i)
        {
            spillTree->GetEntry(i);
            if(spill.goodSpill())  spillBank.insert(map<int, Spill>::value_type(spill.spillID, spill));
        }
    }

    TString query; // string used for queries










    // query for track data for triggers FPGA4 and NIM3
    query = Form("SELECT T.numHits,T.numHitsSt1,T.numHitsSt2,T.numHitsSt3,T.numHitsSt4H,T.numHitsSt4V,"
        "T.chisq,T.chisq_target,T.chisq_dump,T.chisq_upstream,"
        "T.x1,T.y1,T.z1,T.x3,T.y3,T.z3,T.x0,T.y0,T.z0,T.xT,T.yT,"
        "T.xD,T.yD,T.px1,T.py1,T.pz1,T.px3,T.py3,T.pz3,T.px0,T.py0,T.pz0,T.pxT,T.pyT,T.pzT,T.pxD,T.pyD,T.pzD,T.roadID,"
        "T.tx_PT,T.ty_PT,T.thbend,T.kmstatus,T.z0x,T.z0y,T.charge,"
        "T.trackID,T.runID,T.spillID,T.eventID "
        "FROM %s.kTrack%s AS T, %s.Event AS E "
        "WHERE T.spillID=E.spillID AND T.eventID=E.eventID AND (E.MATRIX4=1 OR E.NIM3=1) %s "
        "ORDER BY spillID %s",
        schema_ktrack.Data(), suffix.Data(), schema_main.Data(), sql_cut_track.Data(), sql_extra.Data());







    cout << " -- Query for singles: " << endl << query << endl;
    TSQLResult* res_track = server->Query(query);
    int nTracksRaw = res_track->GetRowCount();
    cout << " -- Query completed, total singles: " << nTracksRaw << endl;

    long long skipped = 0;
    long long bad_queries = 0;
    long long bad_spills = 0;
    spill.spillID = -1;
    bool badSpillFlag = false;
    for(int i = 0; i < nTracksRaw; ++i)
    {
        if(i % 1000 == 0)
        {
            cout << "Converting track " << i << "/" << nTracksRaw << endl;
        }

        //basic track info
        TSQLRow* row_track = res_track->Next();

        track.pxv = ERR_VAL_G;
        track.pyv = ERR_VAL_G;
        track.pzv = ERR_VAL_G;

        track.nHits     = getInt(row_track->GetField(0));
        track.nHitsSt1  = getInt(row_track->GetField(1));
        track.nHitsSt2  = getInt(row_track->GetField(2));
        track.nHitsSt3  = getInt(row_track->GetField(3));
        track.nHitsSt4H = getInt(row_track->GetField(4));
        track.nHitsSt4V = getInt(row_track->GetField(5));
        track.chisq          = getFloat(row_track->GetField(6));
        track.chisq_target   = getFloat(row_track->GetField(7));
        track.chisq_dump     = getFloat(row_track->GetField(8));
        track.chisq_upstream = getFloat(row_track->GetField(9));
        track.x1    = getFloat(row_track->GetField(10));
        track.y1    = getFloat(row_track->GetField(11));
        track.z1    = getFloat(row_track->GetField(12));
        track.x3    = getFloat(row_track->GetField(13));
        track.y3    = getFloat(row_track->GetField(14));
        track.z3    = getFloat(row_track->GetField(15));
        track.x0    = getFloat(row_track->GetField(16));
        track.y0    = getFloat(row_track->GetField(17));
        track.z0    = getFloat(row_track->GetField(18));
        track.xT    = getFloat(row_track->GetField(19));
        track.yT    = getFloat(row_track->GetField(20));
        track.zT    = -129.;
        track.xD    = getFloat(row_track->GetField(21));
        track.yD    = getFloat(row_track->GetField(22));
        track.zD    = 42.;
        track.px1   = getFloat(row_track->GetField(23));
        track.py1   = getFloat(row_track->GetField(24));
        track.pz1   = getFloat(row_track->GetField(25));
        track.px3   = getFloat(row_track->GetField(26));
        track.py3   = getFloat(row_track->GetField(27));
        track.pz3   = getFloat(row_track->GetField(28));
        track.px0   = getFloat(row_track->GetField(29));
        track.py0   = getFloat(row_track->GetField(30));
        track.pz0   = getFloat(row_track->GetField(31));
        track.pxT   = getFloat(row_track->GetField(32));
        track.pyT   = getFloat(row_track->GetField(33));
        track.pzT   = getFloat(row_track->GetField(34));
        track.pxD   = getFloat(row_track->GetField(35));
        track.pyD   = getFloat(row_track->GetField(36));
        track.pzD   = getFloat(row_track->GetField(37));
        track.roadID = getInt(row_track->GetField(38));
        track.tx_PT  = getFloat(row_track->GetField(39));
        track.ty_PT  = getFloat(row_track->GetField(40));
        track.thbend = getFloat(row_track->GetField(41));
        track.kmstatus = getInt(row_track->GetField(42));
        track.z0x    = getInt(row_track->GetField(43));
        track.z0y    = getInt(row_track->GetField(44));
        track.charge  = getInt(row_track->GetField(45));

        track.trackID = getInt(row_track->GetField(46));
        event.runID   = getInt(row_track->GetField(47));
        event.spillID = getInt(row_track->GetField(48));
        event.eventID = getInt(row_track->GetField(49));


        delete row_track;





        // TODO: clean this up for clarity
        //spill info and cuts
        if(event.spillID != spill.spillID) // Real data, new spill
        {
            // NOTE: for previously loaded spills, spill data should already be loaded (is this intended ROOT behavior?)
            event.log("New spill!");
            spill.spillID = event.spillID;

            //check if the spill exists in pre-loaded spills
            if(spillBank.find(event.spillID) == spillBank.end())
            {
                event.log("spill does not exist!");
                badSpillFlag = true;
            }
            else
            {
                spill = spillBank[event.spillID];
                badSpillFlag = false;
            }
        }
        if(!badSpillFlag && !spill.goodTargetPos()) {
            event.log(Form("ERROR: bad target %d)", spill.targetPos));
            badSpillFlag = true;
        }
        if(badSpillFlag) {
            bad_spills += 1;
            continue;
        }


        // write default/error values for missable quantities
        event.weight = 0;
        event.source1 = -1;
        event.source2 = -1;
        event.passed = -1;
        event.Ntr = -1;
        event.Ndi = -1;
        event.intensityP = ERR_VAL_G;
        for(int j = 0; j < NUM_INTENSITY; ++j) event.intensity[j] = ERR_VAL_G;
        for(int j = 0; j < NUM_OCCUPANCY; ++j) event.occupancy[j] = ERR_VAL_G;
        for(int j = 0; j <= NUM_TRIGGER; ++j) event.MATRIX[j] = ERR_VAL_G;
        for(int j = 0; j <= NUM_TRIGGER; ++j) event.NIM[j] = ERR_VAL_G;


        // get event info, QIE, occupancy
        TSQLResult* res_event;
        TSQLRow* row_event;
        // TODO: consolidate duplicated code (status/source/matrix1)
        if(!spill.skipflag) // real data
        {
            // query for tracker multiplicities
            // TODO: Apply also to MC. how to apply to Mix?
            query = Form("SELECT "
                "(SELECT COUNT(*) FROM %s.kTrack%s WHERE eventID=%d),"
                "(SELECT COUNT(*) FROM %s.kDimuon%s WHERE eventID=%d)",
            schema_ktrack.Data(), suffix.Data(), event.eventID, schema_ktrack.Data(), suffix.Data(), event.eventID);
            res_event = server->Query(query);
            if(res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for event multiplicities.");
                delete res_event;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.Ntr = getInt(row_event->GetField(0));
                event.Ndi = getInt(row_event->GetField(1));
                delete row_event;
                delete res_event;
            }


            // TODO: remove sources if always -1?
            query = Form("SELECT kE.status,kE.source1,kE.source2, "
                "E.MATRIX1,E.MATRIX2,E.MATRIX3,E.MATRIX4,E.MATRIX5, "
                "E.NIM1,E.NIM2,E.NIM3,E.NIM4,E.NIM5 "
                "FROM %s.kEvent AS kE, %s.Event AS E  "
                "WHERE kE.runID=%d AND kE.eventID=%d AND E.runID=kE.runID AND E.eventID=kE.eventID",
                schema_ktrack.Data(), schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if(res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for basic event-level data.");
                delete res_event;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.status = getInt(row_event->GetField(0));
                event.source1 = getInt(row_event->GetField(1));
                event.source2 = getInt(row_event->GetField(2));
                //event.MATRIX = getInt(row_event->GetField(3));
                for(int j = 1; j <= NUM_TRIGGER; ++j) event.MATRIX[j] = getInt(row_event->GetField(2+j), ERR_VAL_G);
                for(int j = 1; j <= NUM_TRIGGER; ++j) event.NIM[j] = getInt(row_event->GetField(2+NUM_TRIGGER+j), ERR_VAL_G);
                event.weight = 1;
                delete row_event;
                delete res_event;
            }

            // query for basic QIE data
            query = Form("SELECT "
                "q.`RF-16`,q.`RF-15`,q.`RF-14`,q.`RF-13`,q.`RF-12`,q.`RF-11`,q.`RF-10`,"
                "q.`RF-09`,q.`RF-08`,q.`RF-07`,q.`RF-06`,q.`RF-05`,q.`RF-04`,q.`RF-03`,"
                "q.`RF-02`,q.`RF-01`,q.`RF+00`,q.`RF+01`,q.`RF+02`,q.`RF+03`,q.`RF+04`,"
                "q.`RF+05`,q.`RF+06`,q.`RF+07`,q.`RF+08`,q.`RF+09`,q.`RF+10`,q.`RF+11`,"
                "q.`RF+12`,q.`RF+13`,q.`RF+14`,q.`RF+15`,q.`RF+16` "
                "FROM %s.QIE AS q WHERE q.runID=%d AND q.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for QIE intensities RF-XX.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < NUM_INTENSITY; ++j) event.intensity[j] = getFloat(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }


            // query for basic occupancy data
            query = Form("SELECT o.D1,o.D2,o.D3,o.H1,o.H2,o.H3,o.H4,o.P1,o.P2 "
                "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for basic occupancies.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < 9; ++j) event.occupancy[j] = getInt(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // query for additional QIE data
            query = Form("SELECT q.Intensity_p, q.PotPerQie FROM %s.QIE AS q WHERE q.runID=%d AND q.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                // TODO: get spill-level data from embedded event in case that intensity_p was not precomputed?
                //event.intensityP = event.weightedIntensity(spill.QIEUnit(), spill.pedestal());
                event.log("ERROR: Query failed for QIE field: Intensity_p.");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                event.intensityP = getFloat(row_event->GetField(0), ERR_VAL_G);
                event.PotPerQie = getFloat(row_event->GetField(1), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }

            // query for additional occupancy data
            query = Form("SELECT o.D1L,o.D1R,o.D2L,o.D2R,o.D3L,o.D3R "
                "FROM %s.Occupancy AS o WHERE o.runID=%d AND o.eventID=%d",
                schema_main.Data(), event.runID, event.eventID);
            res_event = server->Query(query);
            if (res_event == NULL || res_event->GetRowCount() != 1) {
                event.log("ERROR: Query failed for L/R occupancies D[1-3][LR].");
                delete res_event;
                bad_queries += 1;
                //continue;
            }
            else {
                row_event = res_event->Next();
                for(int j = 0; j < NUM_OCCUPANCY-9; ++j) event.occupancy[9+j] = getInt(row_event->GetField(j), ERR_VAL_G);
                delete row_event;
                delete res_event;
            }
        }

        //Now save everything
        saveTree->Fill();
        if(saveTree->GetEntries() % 1000 == 0) saveTree->AutoSave("self");
    }
    delete res_track;

    cout << Form("sqlReader finished reading %s tables successfully.", suffix.Data()) << endl;
    cout << "Bad spills: " << bad_spills << endl;
    cout << "Bad queries: " << bad_queries << endl;
    cout << "Skipped: " << skipped << endl;

    saveFile->cd();
    saveTree->Write();
    saveFile->Close();

    return EXIT_SUCCESS;
}
